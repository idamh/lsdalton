def configure(options, input_files, extra_args):
    """
    This function is used by runtest to configure runtest
    at runtime for code specific launch command and file naming.
    """

    from os import path
    from sys import platform

    launcher = 'lsdalton'
    launcher_full_path = path.normpath(path.join(options.binary_dir, launcher))

    if len(input_files) == 2:
        (inp, mol) = input_files
        extra = None
    else:
        # extra can be PCM or PE
        (inp, mol, extra) = input_files

    inp_no_suffix = path.splitext(inp)[0]
    mol_no_suffix = path.splitext(mol)[0]

    command = []
    command.append(launcher_full_path)
    command.append('-noarch')
    command.append('-dal {0}'.format(inp))
    command.append('-mol {0}'.format(mol))
    if extra is not None:
        # if extra is potential input file
        if path.splitext(extra)[1] == '.pot':
            command.append('-pot {0}'.format(extra))
        # else if extra is PCM input file
        elif path.splitext(extra)[1] == '.pcm':
            command.append('-pcm {0}'.format(extra))

    if extra_args is not None:
        command.append(extra_args)

    full_command = ' '.join(command)

    output_prefix = '{0}_{1}'.format(inp_no_suffix, mol_no_suffix)
    if extra is not None:
        output_prefix += '_{0}'.format(extra_no_suffix)

    relative_reference_path = 'reference'

    return launcher, full_command, output_prefix, relative_reference_path
