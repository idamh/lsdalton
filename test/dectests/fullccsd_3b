#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > fullccsd_3.info <<'%EOF%'
   DEC-CCSD energy for full molecule
   --------------------------------
   Molecule:         HF
   Wave Function:    CCSD / cc-pVDZ
   Test Purpose:     Test full molecule CCSD energy using DEC program (Kasper K)
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > fullccsd_3.mol <<'%EOF%'
BASIS
cc-pVDZ


Atomtypes=2  Angstrom Nosymmetry
Charge=9.0 Atoms=1
F         -0.17632        0.11634       -1.09866
Charge=1.0 Atoms=1
H          0.70368        0.11634       -1.09866
%EOF%
#######################################################################
#  DALTON INPUT
#######################################################################
cat > fullccsd_3.dal <<'%EOF%'
**GENERAL
.FORCEGCBASIS
**WAVE FUNCTIONS
.HF
*DENSOPT
.ARH
.START
TRILEVEL
.CONVTHR
1.0d-5
**INFO
.DEBUG_MPI_MEM
**CC
.CCSD
.CCSDforce_scheme
3
.ccThr
1e-7
.MEMORY
1.0
*END OF INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >fullccsd_3.check
cat >>fullccsd_3.check <<'%EOF%'

log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# HF ENERGY 
CRIT1=`$GREP "Final HF energy: * \-99\.893559" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="HF ENERGY NOT CORRECT -"

# CCSD ENERGY
CRIT1=`$GREP "Correlation energy  : * \-0\.20721[8-9]" $log | wc -l`
TEST[2]=`expr   $CRIT1`
CTRL[2]=1
ERROR[2]="CCSD correlation energy is not correct"


# Memory test for total memory                                             
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`         
TEST[3]=`expr  $CRIT1`                                                    
CTRL[3]=1                                                                 
ERROR[3]="Memory leak -"
                                                                          
# Memory test for array4                                                  
CRIT1=`$GREP "Memory in use for array4 * \: * 0.000 * GB" $log | wc -l`   
TEST[4]=`expr  $CRIT1`                                                    
CTRL[4]=1                                                                 
ERROR[4]="Memory leak for array4 -" 

PASSED=1
for i in 1 2 3 4
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%




