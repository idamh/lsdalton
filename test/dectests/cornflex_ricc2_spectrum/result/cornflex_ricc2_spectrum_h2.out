  
     ******************************************************************    
     **********  LSDalton - An electronic structure program  **********    
     ******************************************************************    
  
  
    This is output from LSDalton 1.0
  
  
     IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
     THE FOLLOWING PAPER SHOULD BE CITED:
     
     K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
     L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
     P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
     J. J. Eriksen, P. Ettenhuber, B. Fernandez,
     L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
     A. Halkier, C. Haettig, H. Heiberg,
     T. Helgaker, A. C. Hennum, H. Hettema,
     E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
     M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
     D. Jonsson, P. Joergensen, J. Kauczor,
     S. Kirpekar, T. Kjaergaard, W. Klopper,
     S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
     A. Krapp, K. Kristensen, A. Ligabue,
     O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
     C. Neiss, C. B. Nielsen, P. Norman,
     J. Olsen, J. M. H. Olsen, A. Osted,
     M. J. Packer, F. Pawlowski, T. B. Pedersen,
     P. F. Provasi, S. Reine, Z. Rinkevicius,
     T. A. Ruden, K. Ruud, V. Rybkin,
     P. Salek, C. C. M. Samson, A. Sanchez de Meras,
     T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
     K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
     P. R. Taylor, A. M. Teale, E. I. Tellgren,
     D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
     O. Vahtras, M. A. Watson, D. J. D. Wilson,
     M. Ziolkowski, and H. AAgren,
     "The Dalton quantum chemistry program system",
     WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                               
    LSDalton authors in alphabetical order (main contribution(s) in parenthesis)
    ----------------------------------------------------------------------------
    Vebjoern Bakken,        University of Oslo,        Norway   (Geometry optimizer)
    Radovan Bast,           UiT The Arctic University of Norway (CMake, Testing)
    Pablo Baudin,           Aarhus University,         Denmark  (DEC,CCSD)
    Sonia Coriani,          University of Trieste,     Italy    (Response)
    Patrick Ettenhuber,     Aarhus University,         Denmark  (CCSD)
    Janus Juul Eriksen,     Aarhus University,         Denmark  (CCSD(T), DEC)
    Trygve Helgaker,        University of Oslo,        Norway   (Supervision)
    Stinne Hoest,           Aarhus University,         Denmark  (SCF optimization)
    Ida-Marie Hoeyvik,      Aarhus University,         Denmark  (Orbital localization, SCF opt)
    Robert Izsak,           University of Oslo,        Norway   (ADMM)
    Branislav Jansik,       Aarhus University,         Denmark  (Trilevel, orbital localization)
    Poul Joergensen,        Aarhus University,         Denmark  (Supervision)
    Joanna Kauczor,         Aarhus University,         Denmark  (Response solver)
    Thomas Kjaergaard,      Aarhus University,         Denmark  (RSP, INT, DEC, SCF, Readin, MPI, MAT)
    Andreas Krapp,          University of Oslo,        Norway   (FMM, dispersion-corrected DFT)
    Kasper Kristensen,      Aarhus University,         Denmark  (Response, DEC)
    Patrick Merlot,         University of Oslo,        Norway   (ADMM)
    Cecilie Nygaard,        Aarhus University,         Denmark  (SOEO)
    Jeppe Olsen,            Aarhus University,         Denmark  (Supervision)
    Simen Reine,            University of Oslo,        Norway   (Integrals, geometry optimizer)
    Vladimir Rybkin,        University of Oslo,        Norway   (Geometry optimizer, dynamics)
    Pawel Salek,            KTH Stockholm,             Sweden   (FMM, DFT functionals)
    Andrew M. Teale,        University of Nottingham   England  (E-coefficients)
    Erik Tellgren,          University of Oslo,        Norway   (Density fitting, E-coefficients)
    Andreas J. Thorvaldsen, University of Tromsoe,     Norway   (Response)
    Lea Thoegersen,         Aarhus University,         Denmark  (SCF optimization)
    Mark Watson,            University of Oslo,        Norway   (FMM)
    Marcin Ziolkowski,      Aarhus University,         Denmark  (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
     Who compiled             | kk
     Host                     | kaspers-mbp-2.st.client.au.dk
     System                   | Darwin-15.6.0
     CMake generator          | Unix Makefiles
     Processor                | x86_64
     64-bit integers          | OFF
     MPI                      | OFF
     Fortran compiler         | /usr/local/bin/gfortran
     Fortran compiler version | GNU Fortran (Homebrew gcc 5.3.0) 5.3.0
     C compiler               | /Applications/Xcode.app/Contents/Developer/usr/bin
                              | /gcc
     C compiler version       | Apple LLVM version 8.0.0 (clang-800.0.38)
     C++ compiler             | /Applications/Xcode.app/Contents/Developer/usr/bin
                              | /g++
     C++ compiler version     | Apple LLVM version 8.0.0 (clang-800.0.38)
     Explicit libs            | -framework Accelerate
     Static linking           | OFF
     Last Git revision        | 6e3de76a707736f178656a34e781be9a515ded15
     Git branch               | pablo/lofex
     Configuration time       | 2017-03-27 11:14:46.043948
  

         Start simulation
                      
    ---------------------------------------------------
             PRINTING THE MOLECULE.INP FILE 
    ---------------------------------------------------
                      
    BASIS                                   
    cc-pVDZ Aux=cc-pVDZ-RI                  
    	Energy:      -0.3290367                
                                            
    Atomtypes=1 Nosymmetry Angstrom Charge=0                                                                                
    Charge=1 Atoms=2                                                                                                        
    H    0.000000	0.000000	0.000000                                                                                         
    H    0.000000	0.000000	0.740000                                                                                         
                      
    ---------------------------------------------------
             PRINTING THE LSDALTON.INP FILE 
    ---------------------------------------------------
                      
    **INTEGRALS
    .DENSFIT
    **WAVE FUNCTIONS
    .HF
    *DENSOPT
    .ARH
    .START
    ATOMS
    .CONVDYN
    TIGHT
    .LCM
    **DEC
    .RICC2
    .CCTHR
    1.0e-6
    .USE_SYS_MEM_INFO
    .FROZENCORE
    *CCRESPONSE
    .SPECTRUM
    .MACRO_THR
    1.0e-5
    .THRES
    1.0e-5
    .NEXCIT
    1
    *CORNFLEX
    .NTO_THR
    -1.0
    *END OF INPUT
 
 
                      
    Atoms and basis sets
      Total number of atoms        :      2
      THE  REGULAR   is on R =   1
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 H      1.000 cc-pVDZ                    7        5 [4s1p|2s1p]                                  
          2 H      1.000 cc-pVDZ                    7        5 [4s1p|2s1p]                                  
    ---------------------------------------------------------------------
    total          2                               14       10
    ---------------------------------------------------------------------
                      
                      
    Atoms and basis sets
      Total number of atoms        :      2
      THE  AUXILIARY is on R =   2
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 H      1.000 cc-pVDZ-RI                14       14 [3s2p1d|3s2p1d]                              
          2 H      1.000 cc-pVDZ-RI                14       14 [3s2p1d|3s2p1d]                              
    ---------------------------------------------------------------------
    total          2                               28       28
    ---------------------------------------------------------------------
                      
                      
    Basic Molecule/Basis information
    --------------------------------------------------------------------
      Molecular Charge                   :    0.0000
      Regular basisfunctions             :       10
      Auxiliary basisfunctions           :       28
      Primitive Regular basisfunctions   :       14
      Primitive Auxiliary basisfunctions :       28
    --------------------------------------------------------------------
                      
    Configuration:
    ==============
    This is a Single core calculation. (no OpenMP)
    This is a serial calculation (no MPI)

    You have requested Augmented Roothaan-Hall optimization
    => explicit averaging is turned off!

    Expand trust radius if ratio is larger than:            0.75
    Contract trust radius if ratio is smaller than:         0.25
    On expansion, trust radius is expanded by a factor      1.20
    On contraction, trust radius is contracted by a factor  0.70

    Maximum size of subspace in ARH linear equations:       2

    Density subspace min. method    : None                    
    Density optimization            : Augmented RH optimization          

    Maximum size of Fock/density queue in averaging:   10
 
    Due to the tightend SCF convergence threshold we also tighten the integral Threshold
    with a factor:      0.100000

    Dynamic convergence threshold for gradient:   0.14E-05
    We have detected a Dunnings Basis set so we deactivate the
    use of the Grand Canonical basis, which is normally default.
    The use of Grand Canonical basis can be enforced using the FORCEGCBASIS keyword
    We perform the calculation in the standard input basis
     
    The Overall Screening threshold is set to              :  1.0000E-09
    The Screening threshold used for Coulomb               :  1.0000E-11
    The Screening threshold used for Exchange              :  1.0000E-09
    The Screening threshold used for One-electron operators:  1.0000E-16
    The SCF Convergence Criteria is applied to the gradnorm in OAO basis

    End of configuration!


    First density: Atoms in molecule guess

    Iteration 0 energy:       -0.850277923185
 
    Preparing to do S^1/2 decomposition...
  
    Relative convergence threshold for solver:  1.00000000E-02
    SCF Convergence criteria for gradient norm:  1.41421356E-06
    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift OAO gradient ###
    ******************************************************************************** ###
      1     -1.1274223603    0.00000000000    0.00      0.00000    0.00    3.692E-02 ###
      2     -1.1288195254   -0.00139716503    0.00      0.00000   -0.00    6.172E-03 ###
      3     -1.1288627317   -0.00004320629    0.00      0.00000   -0.00    2.947E-04 ###
      4     -1.1288627760   -0.00000004433    0.00      0.00000   -0.00    5.016E-06 ###
      5     -1.1288627760   -0.00000000001    0.00      0.00000   -0.00    2.348E-10 ###
    SCF converged in      5 iterations
    >>>  CPU Time used in SCF iterations is   0.02 seconds
    >>> wall Time used in SCF iterations is   0.02 seconds

    Total no. of matmuls in SCF optimization:        275

    Number of occupied orbitals:       1
    Number of virtual orbitals:        9

    Number of occupied orbital energies to be found:       1
    Number of virtual orbital energies to be found:        1


    Calculation of HOMO-LUMO gap
    ============================

    Calculation of occupied orbital energies converged in     1 iterations!

    Calculation of virtual orbital energies converged in     5 iterations!

     E(LUMO):                         0.198631 au
    -E(HOMO):                        -0.592573 au
    -------------------------------------------------
     HOMO-LUMO Gap (iteratively):     0.791204 au


    ********************************************************
     it       dE(HF)          exit   RHshift    RHinfo 
    ********************************************************
      1    0.00000000000    0.0000    0.0000    0.0000000
      2   -0.00139716503    0.0000   -0.0000    0.0000000
      3   -0.00004320629    0.0000   -0.0000    0.0000000
      4   -0.00000004433    0.0000   -0.0000    0.0000000
      5   -0.00000000001    0.0000   -0.0000    0.0000000

    ======================================================================
                       LINSCF ITERATIONS:
      It.nr.               Energy                 OAO Gradient norm
    ======================================================================
        1            -1.12742236034298093372      0.369171952148388D-01
        2            -1.12881952537663510228      0.617211311318473D-02
        3            -1.12886273167129780326      0.294665010862027D-03
        4            -1.12886277599944118144      0.501595925491546D-05
        5            -1.12886277601332807308      0.234798139241678D-09

          SCF converged !!!! 
             >>> Final SCF results from LSDALTON <<<


          Final HF energy:                        -1.128862776013
          Nuclear repulsion:                       0.715104335541
          Electronic energy:                      -1.843967111554



    -- Full molecular info --

    FULL: Overall charge of molecule    :      0

    FULL: Number of electrons           :      2
    FULL: Number of atoms               :      2
    FULL: Number of basis func.         :     10
    FULL: Number of aux. basis func.    :     28
    FULL: Number of core orbitals       :      0
    FULL: Number of valence orbitals    :      1
    FULL: Number of occ. orbitals       :      1
    FULL: Number of occ. alpha orbitals :     -3
    FULL: Number of occ. beta  orbitals :      0
    FULL: Number of virt. orbitals      :      9
    FULL: Local memory use type full    :  0.21E-05
    FULL: Distribute matrices           : F
    FULL: Using frozen-core approx.     : T

 Allocate space for molecule%Co on Master use_bg= F
 Allocate space for molecule%Cv on Master use_bg= F
 get_available_memory: System identified to be MAC!
 Available Memory (GB) =       3.0860    
    Memory found by system call to be:    3.086     GB


    =============================================================================
         -- Full molecular Coupled-Cluster calculation -- 
    =============================================================================

    Using canonical orbitals as requested in input!



 ================================================ 
              Full molecular driver               
 ================================================ 


 JAC: Starting guess from orbital energies


 JAC ********************************************************
 JAC        Information for Jacobian eigenvalue solver       
 JAC        ------------------------------------------       
 JAC 
 JAC Number of eigenvalues                 1
 JAC Initial subspace dimension            1
 JAC Maximum subspace dimension            9
 JAC 
 JAC Start guess for eigenvalues
 JAC ---------------------------
 JAC       1    0.7912035650    
 JAC 
 JAC ********************************************************
 JAC
 JAC Jacobian eigenvalue solver
 JAC
 JAC Which   Subspace     Eigenvalue           Residual       Conv?  
 JAC     1       1        0.51866858         0.13498524E-01    F
 JAC     1       2        0.51860445         0.51959660E-03    F
 JAC     1       3        0.51860414         0.67321069E-15    T


      ************************************************************
      *               CCS      excitation energies               *
      ************************************************************

            Exci.    Hartree           eV            cm-1         
              1      0.5186041       14.11194       113820.4    
 Reading file: CCS_R1_001.restart                      






    ******************************************************************************
    *                      Full CCS      calculation is done !                   *
    ******************************************************************************





    ******************************************************************************
    *                             CC ENERGY SUMMARY                              *
    ******************************************************************************

     E: Hartree-Fock energy                            :       -1.1288627760
     E: Correlation energy                             :        0.0000000000
     E: Total CCS energy                               :       -1.1288627760



    CC Memory summary
    -----------------
     Allocated memory for array4   :   0.000     GB
     Memory in use for array4      :   0.000     GB
     Max memory in use for array4  :   0.000     GB
    ------------------


    ------------------------------------------------------
    Total CPU  time used in CC           :        0.104897     s
    Total Wall time used in CC           :        0.254000     s
    ------------------------------------------------------


    Hostname       : kaspers-mbp-2.st.client.au.dk                     
    Job finished   : Date: 27/03/2017   Time: 11:26:03



    =============================================================================
                              -- end of CC program --
    =============================================================================





    -- Full molecular info --

    FULL: Overall charge of molecule    :      0

    FULL: Number of electrons           :      2
    FULL: Number of atoms               :      2
    FULL: Number of basis func.         :     10
    FULL: Number of aux. basis func.    :     28
    FULL: Number of core orbitals       :      0
    FULL: Number of valence orbitals    :      1
    FULL: Number of occ. orbitals       :      1
    FULL: Number of occ. alpha orbitals :     10
    FULL: Number of occ. beta  orbitals :      1
    FULL: Number of virt. orbitals      :      9
    FULL: Local memory use type full    :  0.21E-05
    FULL: Distribute matrices           : F
    FULL: Using frozen-core approx.     : T

 ==================================================================
   LOFEX: Starting MACRO-ITERATION   1 for state   1
 ==================================================================
 Calculating CIS(D)-NTO        1 from scratch...
 Reading file: CCS_R1_001.restart                      
 Total number of AOs / CCS atomic extent      10      10
 RED. DIMENSIONS OCC CCS/TOT       1      1
 RED. DIMENSIONS VIR CCS/TOT       1      6
 CCS eival:        1       1        1.00000000
 LOFEX: CIS(Dp)-NTO init (min.) =        0.00000
 LOFEX: CIS(Dp)-NTO integrals (min.) =        0.00260
 LOFEX: CIS(Dp)-NTO occocc (min.) =        0.00000
 LOFEX: CIS(Dp)-NTO virvir (min.) =        0.00000

 Plot      1 occupied NTO eigenvalues > 0.1 for state     1
 **************************************************************************
 OccNTO       1     1.000000000    


 Plot      1 virtual NTO eigenvalues > 0.1 for state     1
 **************************************************************************
 VirNTO       1    0.9911813038    

 LOFEX: CIS(Dp)-NTO total (min.) =        0.00413
 get_available_memory: System identified to be MAC!
 Available Memory (GB) =       3.0870    
    Memory found by system call to be:    3.087     GB


    =============================================================================
         -- Divide, Expand & Consolidate Coupled-Cluster calculation -- 
    =============================================================================


     ================================================ 
                      DEC-CC driver                   
     ================================================ 


    --------------------------
       DEC input parameters   
    --------------------------

    FOT (Fragment Optimization Threshold)               =        0.10E-03
    Use Pair Estimates to screen pairs                  =            TRUE
    Pair distance cutoff threshold (Angstrom)           =        30.0    
    Use Pair Estimate initialisation number of atom     =     1
    Pair estimate model                                 =           MP2     
    Simple orbital thr.                                 =       0.500E-01
    Expansion step size                                 =               5
    Print level                                         =               0
    Fragment-adapted orbitals                           =           FALSE
    The Integral Screening threshold                    =       0.100E-09
    Memory Distribute Full info                         =           FALSE
    Atomic Fragment Optimization Correction (AFOC)      =           FALSE
     
    DEC orbitals will be generated using  simple Lowdin charge analysis
    Assignment of Molecular Orbitals to Atoms will be based on Lowdin charge analysis
     
    The wave function Model used for Atomic Fragment expansion scheme = MP2     
    This wave function model can be changed using the .FRAGEXPMODEL keyword
    .FRAGEXPMODEL
    .RICC2   
    Wave function Model used for Atomic Fragment reduction scheme = MP2     
    This wave function model can be changed using the .FRAGREDMODEL keyword
    .FRAGREDMODEL
    .RICC2   


    --------------------------
      Coupled-cluster input   
    --------------------------

    Wave function                =           RICC2   
    Maximum number of iterations =             100
    Convergence threshold        =       0.100E-05
    Use CROP                     =            TRUE
    CROP subspace                =               3
    Use Preconditioner           =            TRUE
    Precond. B                   =            TRUE
    Debug mode                   =           FALSE
    CC Solver distributes Memory =           FALSE
    Memory Usage Allowed(GB)     =            3.09
    Backup Time Interval(s)      =          300.00
    Use F12 correction           =           FALSE
     


 ORBITAL EXTENT INFORMATION
 **************************

 Maximum occ orbital extent (#AOs) =      7   -- Orbital index     1
 Maximum virt orbital extent (#AOs) =     10   -- Orbital index     1
 Average occ orbital extent (#AOs) =       7.0000
 Average virt orbital extent (#AOs) =       6.8889



 LOFEX: fragment nocc:      1
 LOFEX: fragment nvir:      9
 LOFEX: fragment nMO :     10
 LOFEX: fragment nbas:     10
 LOFEX: fragment naux:     28


 WARNING: LoFEx fragment include full molecule!!
 CCSOL: MAIN LOOP      :  1.63    s
 CCSOLVER - tot time (min.)            0.02960
 CCSOL: MAIN LOOP      :  2.72    s
 CCSOLVER - tot time (min.)            0.04788
 Reading file: CCS_R1_001.restart                      


 -------------------------------
  Non-linear CC Davidson solver 
 -------------------------------

 Self-consistency threshold =  0.100E-04
 Solve for right eigenvectors
 Input excitation energies:
      0.51860414

 ### Starting Davidson Macro-iterations
 ### ----------------------------------
 ###
 ### State no.  Macro-it   # Micro-it  delta(freq.)     freq. (a.u.)
 ### ---------------------------------------------------------------
 ###     1          1            2     0.253937E-02      0.51606477
 ###     1          2            2     0.401120E-04      0.51610488
 ###     1          3            2     0.635257E-06      0.51610425


 -------------------------------
  Non-linear CC Davidson solver 
 -------------------------------

 Self-consistency threshold =  0.100E-04
 Solve for left eigenvectors
 Input excitation energies:
      0.51610425

 ### Starting Davidson Macro-iterations
 ### ----------------------------------
 ###
 ### State no.  Macro-it   # Micro-it  delta(freq.)     freq. (a.u.)
 ### ---------------------------------------------------------------
 ###     1          1            3     0.989749E-08      0.51610426


 Normalize vectors such that <L|R> = 1 and <R|R> = 1

    State      E [eV]      % singles     w. time [min] 
   ----------------------------------------------------
      1      14.04392         98.44          0.01

 RICC2: <L|R> normalization:           0.01 min.
 CCSOL: MAIN LOOP      :  2.78    s
 CCSOLVER - tot time (min.)            0.04882

 RI-CC2 transition moments module:
 ---------------------------------

      xi  density matrix:       0.02 min.
      eta density matrix:       0.01 min.
      total wall time   :       0.03 min.


 Length gauge was used for the dipole moments integrals


 RICC2    Right transition dipole moments (a.u.):
 ------------------------------------------------

 State            X               Y               Z             Freq.
    1        0.00000000     -0.00000000     -0.90318372      0.51610425


 RICC2    Left  transition dipole moments (a.u.):
 ------------------------------------------------

 State            X               Y               Z             Freq.
    1        0.00000000     -0.00000000     -1.74848192      0.51610425


 RICC2    transition dipole strengths (a.u.):
 --------------------------------------------

 State            X               Y               Z             Freq.
    1        0.00000000      0.00000000      1.57920040      0.51610425


 RICC2    Oscillator strengths (length gauge):
 =============================================

 State        Osc. str.     Freq. (a.u.)         (eV)          (cm-1) 
    1        0.54335469      0.51610425     14.04391866     113271.7863

 LOFEX: oscillator strength converged in iter:   1
 LOFEX: state index         =   1
 LOFEX: excitation energy   =  0.516104
 LOFEX: oscillator strength =  0.543355
 LOFEX: strenght difference =  0.543355
 LOFEX: overlap with guess  =  1.000000
 LOFEX: wall-time (min.)    =      0.30
 LOFEX: full orbital space has been included (quiting)

    CC Memory summary
    -----------------
     Allocated memory for array4   :   0.000     GB
     Memory in use for array4      :   0.000     GB
     Max memory in use for array4  :   0.000     GB
    ------------------


    ------------------------------------------------------
    Total CPU  time used in DEC          :        0.661078     s
    Total Wall time used in DEC          :         18.0680     s
    ------------------------------------------------------


    Hostname       : kaspers-mbp-2.st.client.au.dk                     
    Job finished   : Date: 27/03/2017   Time: 11:26:21



    =============================================================================
                              -- end of DEC program --
    =============================================================================






                                   /\                                  
                _____ _   _   __   \/   __   ______ _      ______      
               / ____| \ | |  \_\_\/\/_/_/  |  ____| |    |  ____|     
              | (___ |  \| |    _\_\/_/_    | |__  | |    | |__  __  __
               \___ \| . ` |   __/_/\_\__   |  __| | |    |  __| \ \/ /
               ____) | |\  |  /_/ /\/\ \_\  | |    | |____| |____ >  < 
              |_____/|_| \_|       /\       |_|    |______|______/_/\_\
                                   \/                                  



      *******************************************************************************
      *                           CCS excitation energies                           *
      *******************************************************************************

            Exci.    Hartree           eV            cm-1
              1      0.5186041       14.11194       113820.4    


      *******************************************************************************
      *                      RICC2   CorNFLEx excitation energies                   *
      *******************************************************************************

            Exci.    Hartree           eV       Overlap w. guess   Osc. Strength   
              1      0.5161043       14.04392      0.9999998          0.5433547

    Total wall-time in LoFEx (includes restarts):       0.31 min.


    Total no. of matmuls used:                       321
    Total no. of Fock/KS matrix evaluations:           6
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Memory statistics          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated memory (TOTAL):         0 byte Should be zero, otherwise a leakage is present
 
      Max allocated memory, TOTAL                          1.051 MB
      Max allocated memory, type(matrix)                  33.888 kB
      Max allocated memory, real(realk)                    1.034 MB
      Max allocated memory, integer                       16.736 kB
      Max allocated memory, logical                        1.476 kB
      Max allocated memory, character                      3.456 kB
      Max allocated memory, AOBATCH                       25.600 kB
      Max allocated memory, DECORBITAL                     1.280 kB
      Max allocated memory, BATCHTOORB                     0.112 kB
      Max allocated memory, ARRAY                          4.920 kB
      Max allocated memory, ODBATCH                        1.936 kB
      Max allocated memory, LSAOTENSOR                     2.432 kB
      Max allocated memory, SLSAOTENSOR                    3.312 kB
      Max allocated memory, ATOMTYPEITEM                 153.056 kB
      Max allocated memory, ATOMITEM                       2.112 kB
      Max allocated memory, LSMATRIX                       2.744 kB
      Max allocated memory, OverlapT                      36.352 kB
      Max allocated memory, linkshell                      0.432 kB
      Max allocated memory, integrand                    114.688 kB
      Max allocated memory, integralitem                 204.800 kB
      Max allocated memory, IntWork                       19.496 kB
      Max allocated memory, Overlap                      689.552 kB
      Max allocated memory, ODitem                         1.408 kB
      Max allocated memory, LStensor                       9.331 kB
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

    Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
    This is a non MPI calculation so naturally no memory is allocated on slaves!
    >>>  CPU Time used in LSDALTON is   1.12 seconds
    >>> wall Time used in LSDALTON is  18.99 seconds

    End simulation
