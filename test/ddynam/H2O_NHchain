#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > H2O_NHchain.info <<'%EOF%'
   H2O_NHchain
   -------------
   Molecule:         H2O
   Wave Function:    PBE / STO-3G

   Test Purpose:     Test direct dynamics of H2O with
   Fock-matrix dynamics
                     
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > H2O_NHchain.mol <<'%EOF%'
BASIS
STO-3G
H20 trajectory

Atomtypes=2
Charge=8.0 Atoms=1
O  2.611850679414D-03   1.390169353183D-01   0.000000000000D+00
Charge=1.0 Atoms=2
H   1.267233393399D+00  -1.086700743158D+00   0.000000000000D+00
H  -1.308685358905D+00  -1.119598894475D+00   0.000000000000D+00
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > H2O_NHchain.dal <<'%EOF%'
**DYNAMI
.NUMTRA
1
.VERLET
.PRINT
6
.TIMESTEP
1.D0
.MAX ITER
5
.PROJGR
.VELOCI
3
 -0.00012895  -0.00029393   0.00028437
 -0.00001556  -0.00022084   0.00004574
 -0.00014834   0.00052977  -0.00017706
.PROJGR
.NHCHAI
.TEMP
300
.BATHFR
0.001
**WAVE FUNCTIONS
.DFT
 BLYP
*DFT INPUT
.DFT-D2
*DENSOPT
.MAXIT
300
*END OF INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL > H2O_NHchain.check
cat >> H2O_NHchain.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * DFT energy\: * \-75\.2735498" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="DFT ENERGY NOT CORRECT -"

# Temperature
CRIT1=`$GREP "Temperature\:    48\.4696" $log | wc -l`
TEST[2]=`expr   $CRIT1`
CTRL[2]=1
ERROR[2]="Temperature -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
