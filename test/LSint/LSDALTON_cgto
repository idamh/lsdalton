#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_cgto.info <<'%EOF%'
   LSDALTON_cgto
   -------------
   Molecule:         SO2 and Water/(6-31G/cc-pVDZ)
   Wave Function:    HF
   Test Purpose:     Check cgto_diff_eri interface
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_cgto.mol <<'%EOF%'
ATOMBASIS
LSint test,
============================
Atomtypes=2 Nosymmetry
Charge=1.0  Atoms=2 Bas=cc-pVDZ
H          -0.337034       -0.861284       -3.013896 
H           0.931113       -0.104860       -3.346471 
Charge=8.0 Atoms=1  Bas=cc-pVDZ
O          -0.009356        0.015216       -3.215161 
%EOF%
#Atomtypes=4 Nosymmetry
#Charge=8.0 Atoms=1  Bas=cc-pVDZ
#O          -1.244255        0.015216        1.446161 
#Charge=8.0 Atoms=2  Bas=6-31G
#O           1.225543        0.015216        1.446161 
#O          -0.009356        0.015216       -3.215161 
#Charge=16.0 Atoms=1 Bas=6-31G
#S          -0.009356        0.015216        0.723517 
#Charge=1.0  Atoms=2 Bas=cc-pVDZ
#H          -0.337034       -0.861284       -3.013896 
#H           0.931113       -0.104860       -3.346471 

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_cgto.dal <<'%EOF%'
**GENERAL
.NOGCBASIS
**INTEGRALS
.DEBUGGEN1INT
.DEBUGCGTODIFF
.NOFAMILY
**WAVE FUNCTIONS
.HF
*DENSOPT
.RH
.DIIS
.START
H1DIAG
.MAXIT
1
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_cgto.check
cat >> LSDALTON_cgto.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

CRIT1=`$GREP "Energy Contribution From cgto_diff_eri_DGD_Econt * " $log | wc -l`
CRIT2=`$GREP "Energy Contribution From Thermite Econt * " $log | wc -l`
TEST[1]=`expr   $CRIT1 \+ $CRIT2`
CTRL[1]=2
ERROR[1]="Energy Contribution not correct"

CRIT1=`$GREP "CGTO Energy Cont Successful" $log | wc -l`
TEST[2]=`expr   $CRIT1 `
CTRL[2]=1
ERROR[2]="Energy Contribution not correct"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[4]=`expr  $CRIT1`
CTRL[4]=0
ERROR[4]="MPI Memory leak -"

PASSED=1
for i in 1 2 3 4
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
