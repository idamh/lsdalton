#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_pass.info <<'%EOF%'
   LSDALTON_pass
   -------------
   Molecule:         C10H2 molecule
   Wave Function:    HF/STO-3G
   Test Purpose:     Check integrals for huge number of passes ensures we 
                     enter the entire loop structure (also npass>maxpasses)
%EOF%
#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_pass.mol <<'%EOF%'
BASIS
STO-3G
C10H2 molecule
Structure is optimized using b3lyp/sto-3g.
Atomtypes=2
Charge=1. Atoms=2
H   0.000000    0.000000    6.899264
H   0.000000    0.000000   -6.899264
Charge=6. Atoms=10
C   0.000000    0.000000    0.616310
C   0.000000    0.000000   -0.616310
C   0.000000    0.000000    1.987676
C   0.000000    0.000000   -1.987676
C   0.000000    0.000000    3.217714
C   0.000000    0.000000   -3.217714
C   0.000000    0.000000    4.600852
C   0.000000    0.000000   -4.600852
C   0.000000    0.000000    5.816819
C   0.000000    0.000000   -5.816819
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_pass.dal <<'%EOF%'
**INTEGRALS
.THRESH
1.D-12
.FTUVMAXPRIM
20
.MAXPASSES
10
**WAVE FUNCTIONS
.HF
*DENSOPT
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_pass.check
cat >> LSDALTON_pass.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * HF energy\: * \-353\.6535720" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="HF ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=0
ERROR[3]="MPI Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
