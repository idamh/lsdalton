#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_ethane_dfK_dft.info <<'%EOF%'
   dft_lda_molhes 
   -------------
   Molecule:         Ethane
   Wave Function:    DFT/ B3LYP / 6-31G / df-def2
   Test Purpose:     Test df K (DF-K/RI-K) energy
                     
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_ethane_dfK_dft.mol <<'%EOF%'
BASIS
6-31G Aux=df-def2
LDA molecular without symmetry
Ethane 
Atomtypes=2 Nosymmetry
Charge=6. Atoms=2
C1      1.4496747688     -0.0000010117     0.0000000000
C2     -1.4496747688      0.0000010117     0.0000000000
Charge=1.  Atoms=6
H1      2.1888645654     -0.9728762646     1.6850699397
H2      2.1888645654     -0.9728762646    -1.6850699397
H3     -2.1888645654      0.9728762646     1.6850699397
H4     -2.1888645654      0.9728762646    -1.6850699397
H5      2.1888617992      1.9457507963     0.0000000000
H6     -2.1888617992     -1.9457507963     0.0000000000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_ethane_dfK_dft.dal <<'%EOF%'
**INTEGRAL
#.DENSFIT
.DF-K
**WAVE FUNCTIONS
.DFT
B3LYP
*DFT INPUT
.GRID TYPE
 BECKEORIG LMG
.RADINT
1.0D-11
.ANGINT
31
*DENSOPT
.RH
.DIIS
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL > LSDALTON_ethane_dfK_dft.check
cat >> LSDALTON_ethane_dfK_dft.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * DFT energy\: * \-79\.74736736" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="DFT ENERGY NOT CORRECT -"

# Memory test
CRIT2=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT2`
CTRL[2]=1
ERROR[2]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=0
ERROR[3]="MPI Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
