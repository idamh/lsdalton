#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION                                                   #
#######################################################################
cat > LSDALTON_camb3lyp2.info <<'%EOF%'
   LSDALTON_camb3lyp2
   ------------------------
   Molecule:         5 HCN molecules placed 20 atomic units apart
   Wave Function:    CAMB3LYP/STO-2G  alpha=0.21 beta=0.79 mu=0.45
   Test Purpose:     test manual setting of alpha, beta and mu
%EOF%

#######################################################################
#  MOLECULE INPUT                                                     #
#######################################################################
cat > LSDALTON_camb3lyp2.mol <<'%EOF%'
BASIS
STO-2G
5 HCN molecules placed 20 atomic units apart
STRUCTURE IS NOT OPTIMIZED. STO-2G basis 
Atomtypes=3 Nosymmetry
Charge=1. Atoms=5
H   0.0   0.0    -1.0 
H   0.0  20.0    -1.0 
H   0.0  40.0    -1.0 
H   0.0  60.0    -1.0 
H   0.0  80.0    -1.0 
Charge=7. Atoms=5
N   0.0   0.0     1.5
N   0.0  20.0     1.5
N   0.0  40.0     1.5
N   0.0  60.0     1.5
N   0.0  80.0     1.5
Charge=6. Atoms=5
C   0.0   0.0     0.0
C   0.0  20.0     0.0
C   0.0  40.0     0.0
C   0.0  60.0     0.0
C   0.0  80.0     0.0
%EOF%

#######################################################################
#  DALTON INPUT                                                       #
#######################################################################
cat > LSDALTON_camb3lyp2.dal <<'%EOF%'
**GENERAL
.TESTMPICOPY
**WAVE FUNCTIONS
.DFT
 CAMB3LYP alpha=0.21 beta=0.79 mu=0.45          
*DFT INPUT
.GRID TYPE
 BECKEORIG LMG
.RADINT
1.0D-11
.ANGINT
31
*DENSOPT
.RH
.DIIS
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT                                                       #
#######################################################################
echo $CHECK_SHELL > LSDALTON_camb3lyp2.check
cat >> LSDALTON_camb3lyp2.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# FINAL ENERGY test
CRIT1=`$GREP "Final DFT energy\: * \-437\.7034090" $log | wc -l`
TEST[1]=`expr	$CRIT1`
CTRL[1]=1
ERROR[1]="FINAL DFT ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

# SCF CONVERGENCE test level 3
CRIT1=`$GREP " 1 * \-437\.59313" $log | wc -l`
CRIT2=`$GREP " 2 * \-437\.683058" $log | wc -l`
CRIT3=`$GREP " 3 * \-437\.703294" $log | wc -l`
CRIT4=`$GREP " 4 * \-437\.703408" $log | wc -l`
CRIT5=`$GREP " 5 * \-437\.7034090" $log | wc -l`
TEST[3]=`expr	$CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5`
CTRL[3]=10
ERROR[3]="DFT CONVERGENCE NOT CORRECT -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
