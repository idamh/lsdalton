#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_INCREM2.info <<'%EOF%'
   LSDALTON_INCREM2
   -------------
   Molecule:         SH2
   Wave Function:    HF/6-31G
   Test Purpose:     Check incremental F build in LSDALTON
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_INCREM2.mol <<'%EOF%'
BASIS
6-31G


Atomtypes=2 Charge=0 Nosymmetry Angstrom
Charge=16.0 Atoms=1
S          0.0000       0.0000  0.0000
Charge=1.0 Atoms=2
H         0.0000        0.9569  0.9208
H          0.0000       -0.9569 0.9208
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_INCREM2.dal <<'%EOF%'
**GENERAL
.NOGCBASIS
**INTEGRALS
.THRESH
1.D-9
**WAVE FUNCTIONS
.HF
*DENSOPT
.ARH
.START
TRILEVEL
.CONVTHR
1.0d-5
.INCREM
.LCM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_INCREM2.check
cat >> LSDALTON_INCREM2.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * HF energy\: * \-398\.62632890" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="DFT ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

PASSED=1
for i in 1 2
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
