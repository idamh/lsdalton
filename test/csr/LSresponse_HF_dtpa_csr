#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSresponse_HF_dtpa_csr.info <<'%EOF%'
   LSresponse_HF_dtpa_csr
   -------------------
   Molecule:         CH2O
   Wave Function:    HF / STO-3G
   Test Purpose:     Test damped two-photon absorption in LSDALTON (Kasper K).
                     
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSresponse_HF_dtpa_csr.mol <<'%EOF%'
BASIS
STO-3G


Atomtypes=3 Nosymmetry Angstrom
Charge=6.0 Atoms=1
C          0.01306        0.00032        0.00148
Charge=8.0 Atoms=1
O         -1.18084       -0.02983       -0.13594
Charge=1.0 Atoms=2
H          0.48384       -0.15591        0.97346
H          0.68393        0.18542       -0.83900
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSresponse_HF_dtpa_csr.dal <<'%EOF%'
**WAVE FUNCTIONS
.HF
*DENSOPT
.RH                                                                                       
.DIIS
.NVEC
8
.CONVTHR
1.0D-6
**RESPONS
*DAMPED_TPA
.OPFREQ
1
0.07729649
.GAMMA
0.001
*END OF INPUT  
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL > LSresponse_HF_dtpa_csr.check
cat >> LSresponse_HF_dtpa_csr.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * HF energy\: * \-112\.35351" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="HF ENERGY NOT CORRECT -"

# Memory test
CRIT2=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT2`
CTRL[2]=1
ERROR[2]="Memory leak -"

# Isotropically averaged TPA
CRIT1=`$GREP "0\.7729649E\-01 * 218\.27[0-9][0-9] * 327\.41[0-9][0-9]" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="Error in damped TPA"


PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
