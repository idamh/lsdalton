\chapter{Installation of \lsdalton}\label{ch:install}

\section{Installation instructions}\index{installation instructions}\label{sec:cmake-installation}

{\lsdalton} is configured using CMake, typically via the setup script,
and subsequently compiled using make or gmake.

Please consult \verb|http://dalton-installation.readthedocs.org| for details.
Help and support from the Dalton community is available at \verb|http://daltonprogram.org/forum|.

\section{Hardware/software supported}
{\lsdalton} can be run on a variety of systems running the UNIX operating system.
The current release of the program supports Linux\index{Linux}, Cray\index{Cray}, SGI\index{SGI},
and MacOS\index{MacOSX} using GNU, Intel, PGI and XL compilers.

The program is written in FORTRAN~90\index{FORTRAN~90} and C\index{C}, with machine dependencies
isolated using C preprocessor directives\index{C preprocessor}.  All
floating-point computations are performed in 64-bit precision.

The program should be portable to other UNIX platforms\index{porting}.  Users
who port the codes to other platforms are encouraged to communicate any
required changes in the original source with the appropriate C preprocessor
directives to the authors.

\section{Source files}\label{sec:source}

The \latestrelease\ program suite is distributed as a \verb|tar|
file obtainable from
the Dalton homepage at \verb|http://www.daltonprogram.org|.
After you have downloaded the file extract it with:
\begin{verbatim}
tar xvzf DALTON-2015.*-Source.tar.gz
\end{verbatim}
Most of the extracted subdirectories under \verb|LSDALTON/*| contain source code for the different
sections constituting the {\lsdalton} program.
Furthermore, there is a directory containing various public domain routines (\verb|pdpack|), a set of test jobs including reference output files (\verb|test|), a directory
containing some useful pre- and post-processing programs supplied to us from
various users (\verb|tools|), and finally this documentation (\verb|doc_LSDALTON|). 

In addition to the above directories, the main dalton directory contain several
files that support the CMake build mechanism (\verb|CMakeLists.txt| and \verb|cmake/*|) as well as 
a directory containing all the basis sets supplied with this distribution (\verb|basis|).

\section{Installing the program using the Makefile}\label{sec:Makefile}
\index{Makefile}
\index{installation!Makefile}
\index{installation!Makefile.config}

The {\lsdalton} program can, in addition to the standard 
standard CMake method (see section \ref{sec:cmake-installation}), be
built using a distributed Makefile and configure script.

The main \verb|.../LSDALTON| directory contains a shell script (\verb|configure|).
Based on the automatic detection of the architecture, the
script will try to build a suitable
\verb|Makefile.config|\index{Makefile.config} on the
basis of what kind of mathematical libraries are found, and user
input. Thus, to execute the script, type
\begin{verbatim}
> ./configure 
\end{verbatim}

The script will now try to detect the architecture, which usually works fine
for most common platforms. However, if for some reason this is not possible, you
may choose one of the supported architectures from a list (currently comprising
only three architectures: aix,linux, and darwin):

Although this script is in most cases capable of making a correct
\verb|Makefile.config|, we always recommend users to check the created
\verb|Makefile.config| against local system set-up. 
In particular, check that \verb|Makefile.config| contains the path(s)
to the proper mathematical libraries. The program runs many times slower
if not linked to any mathematical libraries (i.e. using {\lsdalton}'s own 
``hand coded'' matrix multiplications etc.)

During the execution of the \verb|configure| script, you will be
asked to decide which compiler you would like to use. 

The configure script searches for compilers in a ordered fashion, and in 
each instance you can choose to accept the compiler. When a suitable compiler has been found, you will be asked a few questions:

\begin{enumerate}

\item {\bf Do you want to use debug compiler options ? [y/N]}

Specifies whether or not you would like to use low optimization level and debug options, useful for developments. The y and N in the square braket indicate the two options. y=Yes, N=No. The Capital letter is the default. In this case the default (chosen by pressing enter) is No.

\item {\bf Do you want to install the program with OpenMP parallelization? [Y/n]}

Specifies whether or not you would like to compile the code using openMP (utilizing
several cores on a single node.

Note that the configure script do not support MPI parallelization and the resulting Makefile.config have to be manually changed to use MPI.

Default is Y=Yes.

 
%\item {\bf Do you want to enable 64-bit integers (for large-scale calculations)?}
%
%Integers will use 64 bits instead of 32 bits, requiring more memory, but making it
%possible to do larger calculations.
%
%\item {\bf Do you want to enable Compressed-Sparse Row (CSR) matrices (for large-scale calculations)?}
%
%When using CSR matrices, only non-zero elements in matrices are stored. When matrices
%are sparse enough, linear scaling in both memory and cpu time is obtained. However, for
%small or non-sparse systems, there is a considerable overhead in cpu time compared to 
%using standard dense matrices.

\item {\bf Does the Fortran compiler support the Fortran 2003 features like procedure pointers and iso\_c\_bindings?}

\item {\bf Use current directory as installation directory for binaries and scripts?}
\index{install directory}
\index{installation!install directory}

Denotes the directory where the executable and the run script will be moved to.

\item {\bf Use basis as default basis set directory?}
\index{basis set!library}
\index{installation!basis set library}

This defines the directory where the program will look for the
basis sets supplied with the distribution, and this need to be
changed according to the local directory structure. We recommend
that the basis sets in this directory are {\em not} changed.
Changes to the basis set should be done in a separate
directory, and you may then supply this basis set directory to the program
at execution time using the command.
\begin{verbatim}
> export BASDIR=path/to/your/basissetdir
\end{verbatim}
In order to ensure that your basis set is read correctly we recommend to use the
\begin{verbatim}
**INTEGRALS
.BASPRINT 
6
\end{verbatim}
Which will print the exponents and contraction coefficients read from file as 
well as the normalized basis used in {\lsdalton}.  

\item {\bf Default scratch space}
\index{scratch space}
\index{installation!scratch space}

Determines the default head scratch
directory where temporary files will be placed. This value will be put
in the \verb|lsdalton| run script. However, note that jobs will be run in
a subdirectory of this head scratch-directory, according to the name
of the job files. If \verb|/work| or \verb|/scratch| is defined in the
local directory structure, the script will normally suggest
\verb|/work/$USER| or \verb|/scratch/$USER| as default head scratch space.

\end{enumerate}

Compiler options will be supplied in \verb|Makefile.config|.
When  \verb|Makefile.config|\index{Makefile.config} has been properly
created and checked
to agree with local system set-up, all that is needed in order to
to build an executable\index{building an executable} version of the
code is to type
(in the same directory as the \verb|Makefile.config| file):
\begin{verbatim}
> make
\end{verbatim}

\section{The Makefile.config}\label{sec:makefileconfig}
%\index{test suite}\index{installation!test suite}

In this section we give a brief introduction to the Makefile.config, which contains compiler instructions for the compilation of the program. 
The introduction is directed to new users with a limited understanding of Makefiles and we only discuss subjects that may be relevant. 
Note that the configure script will in most cases provided a Makefile.config which works and no modifications are required from the user. 
Depending on the version of math libraries, the configure script may not be able to find these and it may be required to manually 
add paths to mathematical libraries. 

\subsection{Intel compiler}\label{ifortMakefile}
The first lines of a Makefile.config using the intel ifort/icc compilers may look like this
\begin{verbatim}
1   ARCH       = linux
2   FMMDIR      = mm
3   #
4   #
5   CPPFLAGS      = -DSYS_LINUX -D_FILE_OFFSET_BITS=64
    -D'INSTALL_BASDIR="/home/user/lsdalton/basis/"' -DVAR_LINSCA 
    -DVAR_OMP -DCOMPILER_UNDERSTANDS_FORTRAN_2003 -DVAR_IFORT
6   F77            = ifort
7   F90            = ifort
8   FLNK           = ifort
9   CC             = icc
10  CXX            = icpc
11  RM             = rm -f
12  FFLAGS         = -O3 -ip -w
13  F90OPTFLAGS    = -O3 -ip -w -fpp1 -openmp
14  SAFEFFLAGS     = -O2 -w
15  CFLAGS         = -O3 -ip -restrict -DRESTRICT=restrict -openmp -DUSE_UNDERSCORES
16  CXXFLAGS       = -Wall -g -wd981 -wd279 -wd383 -vec-report0 -wd1572 -wd177 -fno-rtti 
-fno-exceptions -O3 -openmp -DUSE_UNDERSCORES
17  INCLUDES       = 
18  LIBS           = -lstdc++ -openmp -liomp5 -lpthread 
19  INSTALLDIR     = /home/user/lsdalton
20  PDPACK_EXTRAS  = linpack.o eispack.o cholesky.o invroutines.o 
gp_dlapack.o gp_zlapack.o gp_dblas3.o gp_dblas2.o gp_dblas1.o gp_zblas.o
\end{verbatim}
We now describe this Makefile.config file line by line, before we discuss a number of issues.

\begin{itemize}
\item Line 1:  ARCH is set to the architecture. (for an aix architecture ARCH=rs6000, for a Mac computer ARCH=darwin, these will be discussed shortly)
\item Line 2:  The directory of the Fast multipole moment code
\item Line 5:  CPPFLAGS is a list of precompiler flags which is required for the program to compile correctly
  \begin{description}
  \item{-DSYS\_LINUX} tells the {\lsdalton} program that this is a linux architecture
%  \item{-DFILE\_OFFSET\_BITS} Stinne XXXX ? 
  \item{-DINSTALL\_BASDIR} The basisset installation directory
  \item{-DVAR\_LINSCA} Is a mandatory keyword!
  \item{-DVAR\_OMP} tells the {\lsdalton} program that OpenMP is used
  \item{-DCOMPILER\_UNDERSTANDS\_FORTRAN\_2003} tells the {\lsdalton} program that the 
Fortran compiler support the Fortran 2003 features like procedure pointers and iso\_c\_bindings
  \item{-DVAR\_IFORT} tells the {\lsdalton} program that the Fortran compiler is an Intel ifort compiler
  \end{description}
  Usually this line works out of the box
\item Line 6:  specifies the compiler used to compile fortran 77 files (obsolete)
\item Line 7:  specifies the compiler used to compile fortran 90 files 
\item Line 8:  specifies the compiler used for linking
\item Line 9:  specifies the compiler used to compile c files
\item Line 10:  specifies the compiler used to compile c files
\item Line 11:  specifies the command to be used when cleaning
\item Line 12-16:  specifies compiler flags that should be used to compile fortran files. Naturally these flags depend on the compiler. Here we use a intel fortran compiler (ifort)
  \begin{description}
  \item{-O3} Specifies the code optimization for applications. Possible options are O0, O1, O2, or O3, with O2 as default
%msse2
\item{-ip} Additional interprocedural optimizations for single-file compilation are enabled.
\item{-w} Disables all warning messages (same as -nowarn)
\item{-fpp1} Runs the Fortran preprocessor on source files before compilation. Syntax is fpp[n], where n can be 0, 1, 2, or 3, where 1, 2, or 3 tells the compiler to run the preprocessor
\item{-openmp} Enables the parallelizer to generate multi-threaded code based on the OpenMP directives. This option sets option automatic, which causes all local, non-saved variables to be allocated to the run-time stack, this may result in a lack of stack-memory as discussed below.
  \end{description}
\item Line 15:  specifies compiler flags that should be used to compile C files. Naturally these flags depend on the compiler. Here we use a intel fortran compiler (icc). Some of the compiler options have already been explained under the ifort options and they will not be explained again.
\item Line 16:  specifies compiler flags that should be used to compile C++ files. 
  \begin{description}
  \item{-restrict} Pointer disambiguation is enabled with the restrict qualifier.
  \item{-DRESTRICT=restrict} Precompiler flag used in DFT code
  \item{-DUSE\_UNDERSCORES} required to define the communication between fortran and C files.
  \end{description}
\item Line 17: Empty by default. Used if for some reason, any additional directories should be included.
\item Line 18: Libraries you wish to include. -openmp (used to be -lguide) and -lpthread are required for openmp parallelization. See Sections \ref{mkl:ifort1} and \ref{mkl:ifort2} for more details on how to link to mkl and other libraries. If you use the old version of ifort you need to replace -openmp with -lguide. 
\item Line 19: the install directory 
\item Line 20: \lsdalton\ provides its own blas and lapack libraries, located in the pdpack directory. These should only be used
if no mathematical libraries are available, since they slow down the code significantly. 

In case you link to an external library, like the intel mkl library, you only need to include "linpack.o eispack.o cholesky.o invroutines.o".

In case you do not link to an external library, you need to include "linpack.o eispack.o cholesky.o invroutines.o gp\_dlapack.o gp\_zlapack.o gp\_dblas3.o gp\_dblas2.o gp\_dblas1.o gp\_zblas.o."
\end{itemize}

\subsubsection{OpenMP}
Note that when compiling using the -openmp option it may be required to increase the stack memory. On a linux machine the size of the stack-memory can be viewed using the command
\begin{verbatim}
> ulimit -a
\end{verbatim}
and changed by
\begin{verbatim}
> ulimit -s newstacksize
\end{verbatim}
The newstacksize should be a large number or unlimited.

%linking to mkl
\subsubsection{Math Library: MKL}\label{mkl:ifort1}
Linking to a math library is crucial for optimal performance. The linking 
depends on the library available. We encourage to first compile with the 
default Makefile.config composed by the configure script. This ensures that 
your chosen compiler works with the {\lsdalton} source code,
and then you can try to link to a math library.
There is a number of different ways that can be used to 
link {\lsdalton} with the mkl library.
An example of a brute force way is the following:
\begin{verbatim}
16  LIBS           = 
/opt/intel/Compiler/11.1/056/mkl/lib/em64t/libmkl_solver_lp64_sequential.a 
-Wl,--start-group /opt/intel/Compiler/11.1/056/mkl/lib/em64t/libmkl_intel_lp64.a 
/opt/intel/Compiler/11.1/056/mkl/lib/em64t/libmkl_sequential.a 
/opt/intel/Compiler/11.1/056/mkl/lib/em64t/libmkl_core.a -Wl,--end-group 
-lguide -lpthread
17  INSTALLDIR     = /home/user/lsdalton
18  PDPACK_EXTRAS  = linpack.o eispack.o
\end{verbatim}
for the sequential linking (no OpenMP), assuming that the intel compiler is 
located at /opt/intel/Compiler/11.1/056 and that the mkl library is located 
at /opt/intel/Compiler/11.1/056.

This link line have been obtained from the "Intel Math Kernel Library Link Line Advisor" 
at http://software.intel.com/en-us/articles/intel-mkl-link-line-advisor.

The OpenMP version of the library can be used in the following way.
\begin{verbatim}
16  LIBS           = 
/opt/intel/Compiler/11.1/056/mkl/lib/em64t/libmkl_solver_lp64_sequential.a 
-Wl,--start-group /opt/intel/Compiler/11.1/056/mkl/lib/em64t/libmkl_intel_lp64.a 
/opt/intel/Compiler/11.1/056/mkl/lib/em64t/libmkl_intel_thread.a 
/opt/intel/Compiler/11.1/056/mkl/lib/em64t/libmkl_core.a -Wl,--end-group 
-lguide -lpthread
17  INSTALLDIR     = /home/user/lsdalton
18  PDPACK_EXTRAS  = linpack.o eispack.o
\end{verbatim}
A somewhat more elegant way is to add the MKL directory to the INCLUDE line 
\begin{verbatim}
15  INCLUDES       = -I/home/user/lsdalton/include
16  LIBS           = -I/opt/intel/Compiler/11.1/056/mkl/include -lmkl_intel_lp64 
-lmkl_sequential -lmkl_core
17  INSTALLDIR     = /home/user/lsdalton
18  PDPACK_EXTRAS  = linpack.o eispack.o
\end{verbatim}
or a threaded version
\begin{verbatim}
15  INCLUDES       = -I/home/user/lsdalton/include
16  LIBS           = -I/opt/intel/Compiler/11.1/056/mkl/include -lmkl_intel_lp64 
-lmkl_intel_thread -lmkl_core -lguide -lpthread
17  INSTALLDIR     = /home/user/lsdalton
18  PDPACK_EXTRAS  = linpack.o eispack.o
\end{verbatim}

\subsection{Gfortran/gcc compiler}
The first lines of a Makefile.config using the gfortran/gcc (version 4.5.1) on a linux architecture may look like this
\begin{verbatim}
1   ARCH       = linux
2   FMMDIR      = mm
3   #
4   #
5   CPPFLAGS      = -DSYS_LINUX -D_FILE_OFFSET_BITS=64
    -D'INSTALL_BASDIR="/home/user/lsdalton/basis/"' -DVAR_LINSCA
    -DVAR_OMP -DCOMPILER_UNDERSTANDS_FORTRAN_2003 -DGFORTRAN=472
6   F77            = gfortran
7   F90            = gfortran
8   FLNK           = gfortran
9   CC             = gcc
10  CXX            = g++
11  RM             = rm -f
12  FFLAGS         = -march=x86-64 -O3 -ffast-math -funroll-loops 
-ftree-vectorize -ffloat-store
13  F90OPTFLAGS    = -march=x86-64 -O3 -ffast-math -funroll-loops 
-ftree-vectorize -ffloat-store -I. -x f95-cpp-input -ffloat-store -fopenmp
14  SAFEFFLAGS     = -march=x86-64 -O3 -ffast-math -funroll-loops 
-ftree-vectorize -ffloat-store
15  CFLAGS         = -march=x86-64 -O3 -ffast-math -funroll-loops 
-ftree-vectorize -std=c99 -DRESTRICT=restrict -DFUNDERSCORE=1 -ffloat-store 
-DUSE_UNDERSCORES
16  CXXFLAGS       = -march=x86-64 -g -O3 -Wall -fno-rtti -fno-exceptions -fopenmp -DUSE_UNDERSCORES
17  INCLUDES       = -I/home/user/lsdalton/include 
18  LIBS           = -lstdc++ -lgomp 
19  INSTALLDIR     = /home/user/lsdalton
20  PDPACK_EXTRAS  = linpack.o eispack.o cholesky.o invroutines.o gp_dlapack.o 
    gp_zlapack.o gp_dblas3.o gp_dblas2.o gp_dblas1.o gp_zblas.o
\end{verbatim}
On a MAC computer a Makefile.config using gfortran/gcc could look like this
\begin{verbatim}
1   ARCH       = darwin
2   FMMDIR     = mm
3   #
4   #
5   CPPFLAGS      = -DSYS_LINUX -D_FILE_OFFSET_BITS=64 
    -DVAR_SPLITFILES -DVAR_G77 -DGFORTRAN=472 -DVAR_LINSCA
    -D'INSTALL_BASDIR="/home/user/lsdalton/basis/"' 
    -DVAR_OMP -DCOMPILER_UNDERSTANDS_FORTRAN_2003
6   F77            = gfortran
7   F90            = gfortran
8   FLNK           = gfortran
9   CC             = gcc
10  CXX            = g++
11  RM             = rm -f
12  FFLAGS         = -O3 -ffast-math -fexpensive-optimizations -funroll-loops 
    -ftree-vectorize
13  F90OPTFLAGS    = -O3 -ffast-math -fexpensive-optimizations -funroll-loops 
    -ftree-vectorize -I. -x f95-cpp-input
14  SAFEFFLAGS     = -O2 -ffast-math -fexpensive-optimizations -funroll-loops 
    -ftree-vectorize
15  CFLAGS         = -O3 -ffast-math -fexpensive-optimizations -funroll-loops 
    -ftree-vectorize -std=c99 -DRESTRICT=restrict -DFUNDERSCORE=1 -DUSE_UNDERSCORES
16  CXXFLAGS       = -O3 -Wall -fno-rtti -fno-exceptions -fopenmp -DUSE_UNDERSCORES
17  INCLUDES       = -I/home/user/lsdalton/include 
18  LIBS           = -lstdc++ -lpthread -lm -lgfortranbegin -lgfortran
19  INSTALLDIR     = /home/user/lsdalton
20  PDPACK_EXTRAS  = linpack.o eispack.o cholesky.o invroutines.o gp_dlapack.o 
    gp_zlapack.o gp_dblas3.o gp_dblas2.o gp_dblas1.o gp_zblas.o
\end{verbatim}
We now describe these Makefile.config files in comparison to section \ref{ifortMakefile}.

Line 1 to 5 is unchanged for the linux architecture, but on the MAC machine -DGFORTRAN must be included in the CPPFLAGS. 

Line 6-9 specifies the use of the gfortran/gcc compilers. 

Line 11-13 specify compiler flags and are naturally different for different compilers
\begin{description}
\item{-march=x86-64} This specifies the name of the target architecture. GCC uses this name to determine what kind of instructions it can emit when generating assembly code. In this case it is a 64 bit machine. The use of this compiler flag is not required, but recommended. 
\item{-O3} Specifies the code optimization for applications. Possible options are O0, O1, O2, or O3, with O2 as default
\item{-ffast-math} A Compiler option to optimize floating-point arithmetic 
\item{-funroll-loops} Unroll loops whose number of iterations can be determined at compile time or upon entry to the loop
\item{-ftree-vectorize} Perform loop vectorization on trees. This flag is enabled by default at -O3
\item{fexpensive-optimizations} Perform a number of minor optimizations that are relatively expensive.
\item{-ffloat-store} This may be required depending on the gfortran version, but usually not. it enforces IEEE compliance 
\item{-fopenmp} Enables the parallelizer to generate multi-threaded code based on the OpenMP directives. 
\item{-std=c99} Determines the language standard, in this case C 99 standard. 
  \end{description}
Line 16 are libraries you wish to include. -lgomp are required for openmp parallelization. 

\subsubsection{Math Library: Linux MKL}\label{mkl:ifort2}
Linking to a math library is crucial for optimal performance. The linking 
depends on the library available. We encourage to first compile with the 
default Makefile.config composed by the configure script. This ensures that 
your chosen compiler works with the {\lsdalton} source code,
and then you can try to link to a math library. 
\lsdalton\ provides its own blas and lapack libraries, which is placed in the 
pdpack directory. However it is encouraged to link to an external library, like the intel mkl library, 
then you only need to include linpack.o eispack.o.
In the case that you do not link to an external library, you need to include 
all. linpack.o eispack.o. gp\_dlapack.o 
gp\_zlapack.o gp\_dblas3.o gp\_dblas2.o gp\_dblas1.o gp\_zblas.o.

Note that linking to mkl library using OpenMP requires to use -liomp5 in stead of -lgomp
\begin{verbatim}
17  INCLUDES       = -I/home/user/lsdalton/include 
-I/opt/intel/Compiler/11.1/056/mkl/include
18  LIBS           = -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core 
-lguide -lpthread -liomp5
19  INSTALLDIR     = /home/user/lsdalton
20  PDPACK_EXTRAS  = linpack.o eispack.o
\end{verbatim}
or
\begin{verbatim}
17  INCLUDES       = 
18  LIBS           = 
/opt/intel/Compiler/11.1/056/mkl/lib/em64t/libmkl_solver_lp64_sequential.a 
-Wl,--start-group /opt/intel/Compiler/11.1/056/mkl/lib/em64t/libmkl_intel_lp64.a 
/opt/intel/Compiler/11.1/056/mkl/lib/em64t/libmkl_intel_thread.a 
/opt/intel/Compiler/11.1/056/mkl/lib/em64t/libmkl_core.a -Wl,--end-group 
-lguide -lpthread -liomp5 
19  INSTALLDIR     = /home/user/lsdalton
20  PDPACK_EXTRAS  = linpack.o eispack.o
\end{verbatim}

%\subsubsection{Math Library: Mac MKL}\label{mkl:ifort}
%
%XXX
%XXX
%XXX
%
% \subsection{XLF/XLC}
% The xlf/xlc compilers are usually used on AIX architectures and it may look like this for a serial (No OpenMP version)
% \begin{verbatim}
% 1   ARCH       = rs6000
% 2   FMMDIR      = mm
% 3   #
% 4   #
% 5   CPPFLAGS      = -WF,-DSYS_AIX,-D'INSTALL_BASDIR="/home/user/lsdalton/basis/"' -DVAR_LINSCA
% 6   F77            = xlf
% 7   F90            = xlf90
% 8   FLNK           = xlf
% 9   CC             = xlc
% 10  RM             = rm -f
% 11  FFLAGS         = -O3 -qstrict -qarch=auto -qtune=auto -q64 -qextname
% 12  F90OPTFLAGS    = -O3 -qstrict -qarch=auto -qtune=auto -q64 -qextname 
%     -qsuffix=f=f90:cpp=f90 -qlanglvl=90std -qinit=f90ptr 
% 13  SAFEFFLAGS     = -O2 -qstrict -qarch=auto -qtune=auto -q64 -qextname
% 14  CFLAGS         = -O3 -I/home/user/lsdalton/include -DSYS_AIX -D_LARGE_FILES
%     -qlanglvl=stdc99 -DRESTRICT=restrict -qarch=auto -qtune=auto -q64 
%     -DUSE_UNDERSCORES 
% 15  INCLUDES       = -I/home/user/lsdalton/include 
% 16  LIBS           = 
% 17  INSTALLDIR     = /home/user/lsdalton
% 18  PDPACK_EXTRAS  = linpack.o eispack.o gp_dlapack.o gp_zlapack.o 
%     gp_dblas3.o gp_dblas2.o gp_dblas1.o gp_zblas.o
% 19  AR             = ar
% 20  ARFLAGS        = -X64 rvs
% \end{verbatim}
% We now describe this Makefile.config in comparison to section \ref{ifortMakefile}
% Line 1 to 4 is unchanged, but in line 5 the -DSYS\_AIX is needed and a different syntax is used. 

% Line 6-9 specifies the use of the xlf/xlc compilers. 
% Line 11-14 specifies the xlf/xlc recommended compiler option  
% \begin{description}
% \item{-march=x86-64} This specifies the name of the target architecture. GCC uses this name to determine what kind of instructions it can emit when generating assembly code. In this case it is a 64 bit machine
% \item{-O3} Specifies the code optimization for applications. Possible options are O0, O1, O2, O3, O4 or O5, with O2 as default
% \item{-qstrict} Ensures that optimizations done by default at optimization levels -O3 and higher, and, optionally at -O2, do not alter the semantics of a program
% \item{-qarch=auto} specifies the processor architecture for which the code (instructions) should be generated. In this case the specific architecture the compiling machine is automatically detected.
% \item{-qtune=auto} Optimizations are tuned for the platform on which the application is compiled
% \item{-q64} Indicates 64-bit compilation bit mode and, together with the -qarch option, determines the target machines on which the 64-bit executable will run
% \item{-q32} Enables 32-bit compilation mode (or, more briefly, 32-bit mode) support in a 64-bit environment
% \item{-qextname} Adds an underscore to the names of all global entities.
% Used on the OpenMP directives. 
% \item{-qlanglvl=90std} Determines whether source code and compiler options should be checked for conformance to a specific language standard. In this case fortran 90 standard and C 99 standard. 
% \item{-qinit=f90ptr} Makes the initial association status of pointers disassociated.
%   \end{description}
% In line 20 it may be necessary to manual add -X64 depending on the architecture (32 or 64 bit). 

% In case of OpenMP, a number of changes must be made. In line 7, the xlf90\_r compiler is specified. This is the fortran compiler which is related to OpenMP. In addition the -qsmp=omp compiler flags must be used. 
% \begin{verbatim}
% 1   ARCH       = rs6000
% 2   FMMDIR      = mm
% 3   #
% 4   #
% 5   CPPFLAGS      = -WF,-DSYS_AIX,-D'INSTALL_BASDIR="/home/user/lsdalton/basis/"',-DVAR_LINSCA 
% 6   F77            = xlf
% 7   F90            = xlf90_r
% 8   FLNK           = xlf
% 9   CC             = xlc
% 10  RM             = rm -f
% 11  FFLAGS         = -O3 -qstrict -qarch=auto -qtune=auto -q64 -qextname
% 12  F90OPTFLAGS    = -O3 -qstrict -qarch=auto -qtune=auto -q64 -qextname 
%     -qsuffix=f=f90:cpp=f90 -qlanglvl=90std -qinit=f90ptr -qsmp=omp
% 13  SAFEFFLAGS     = -O2 -qstrict -qarch=auto -qtune=auto -q64 -qextname
% 14  CFLAGS         = -O3 -I/home/user/lsdalton/include -DSYS_AIX -D_LARGE_FILES
%     -qlanglvl=stdc99 -DRESTRICT=restrict -qarch=auto -qtune=auto -q64 
%     -DUSE_UNDERSCORES 
% 15  INCLUDES       = -I/home/user/lsdalton/include 
% 16  LIBS           = 
% 17  INSTALLDIR     = /home/user/lsdalton
% 18  PDPACK_EXTRAS  = linpack.o eispack.o gp_dlapack.o gp_zlapack.o 
%     gp_dblas3.o gp_dblas2.o gp_dblas1.o gp_zblas.o
% 19  AR             = ar
% 20  ARFLAGS        = -X64 rvs
% \end{verbatim}

% \subsubsection{Math Library: MASS ESSL}

% For AIX architectures we recommend linking to the 
% IBM Mathematical Acceleration Subsystem (MASS) library, 
% which are shipped with the XL C, XL C/C++, and XL Fortran 
% compiler products. This is done by adding the -lmass 
% -lmassv -L/usr/local/mass flags to line 16. We also recommend 
% the use of Engineering and Scientific Subroutine Library. The 
% ESSL provides a set of highly optimized mathematical subroutines 
% especially tuned for the IBM RISC System/6000 and for IBM Power 4. 
% The library can be used with Fortran, C, and C++ programs. Since 
% blas libraries are found through MASS and ESSL, only the lapack 
% library routines from .../lsdalton are needed ("gp\_zlapack.o 
% gp\_dlapack.o" in PDPACK\_EXTRAS, in addition to "linpack.o eispack.o").
% \begin{verbatim}
% 16  LIBS           = -lessl -lmass -lmassv -L/usr/local/mass
% 17  INSTALLDIR     = /home/user/lsdalton
% 18  PDPACK_EXTRAS  = linpack.o eispack.o gp_zlapack.o gp_dlapack.o
% \end{verbatim}
% In the case of OpenMP special OpenMP libraries must be used
% \begin{verbatim}
% 16  LIBS           = -Llib -L/usr/lib -lxlsmp  -lmass -lmassv -L/usr/local/mass
%  -lpthread -lC -lesslsmp
% 17  INSTALLDIR     = /home/user/lsdalton
% 18  PDPACK_EXTRAS  = linpack.o eispack.o gp_zlapack.o gp_dlapack.o
% \end{verbatim}

\subsection{Using 64-bit integers}
For large scale calculations, it may be necessary to compile the program using 64-bit integers instead of the default 32-bit integers.
To do this, you need to include "-DVAR\_INT64" in the CPPFLAGS (in addition to what is already there). Furthermore, 
the FFLAGS, F90OPTFLAGS, SAFEFFLAGS, and CFLAGS must be changed depending on you compiler. For ifort/icc, you need to add "-i8",
and for XLF/XLC, you need to add "-qintsize=8". The use of 64-bit integers has not been tested for gfortran. Note that you also have
to change the mathematical libraries in LIBS to their 64-bit counterparts. This is not always a simple task, and you may need to ask your
system administrator. 

\subsection{Using Compressed-Sparse Row matrices}
When using Compressed-Sparse Row (CSR) matrices, only non-zero elements in matrices are stored. 
When matrices are sparse enough, linear scaling in both memory and cpu time is obtained. However, for
small or non-sparse systems, there is a considerable overhead in cpu time compared to 
using standard dense matrices, so don't use it unless you need it. You enable CSR by putting
"-DVAR\_MKL" and "-DVAR\_CSR" in your CPPFLAGS (in addition to what is already there. You also need to link
to MKL's CSR library (what we have in \lsdalton\ is an interface to MKL CSR). Furthermore, to run a calculation
using CSR matrices, you always need .CSR under *DENSOPT in the input file LSDALTON.INP (described in detail in
chapters~\ref{ch:starting} and~\ref{ch:keywords}).

% \section{Running the {\lsdalton} test suite}\label{sec:testsuite}
% \index{test suite}\index{installation!test suite}

% To check that {\lsdalton} has been successfully installed, a fairly
% elaborate automatic test suite is provided in the distribution. A test
% script and all the test jobs and reference output files can be found in
% the \verb|.../lsdalton/test| directory. It is highly recommended that all
% these tests be run once the program has been compiled. Depending on
% your hardware, this usually takes 1/2---2 hours.

% The tests can be run one by one or in groups, by using the test script
% \verb|TEST|. Try \verb|TEST -h| to see the different options this
% script takes. 
% To run the
% complete test suite, go to the \verb|.../lsdalton/test| directory and
% type:
% \begin{verbatim}
% > ./TEST all
% \end{verbatim}
% You can follow the progress of the tests directly, but all messages
% are also printed to a log (\verb|TESTLOG| by default). After all the
% tests have completed you should hopefully be presented with the
% message ``ALL TESTS ENDED PROPERLY!''. If not, the complete list of 
% failing test-cases
% will be printed. Please consult the file
% \verb|KNOWN_PROBLEMS| too see if these tests have documented problems
% on your particular platform. 

% Any tests that fail will leave behind the \verb|.mol| and \verb|.dal|
% input-files (these are described in more detail in
% chapter~\ref{ch:starting}), and the output file from the test
% calculation which will have the extension \verb|.log| 
% For all
% successful tests these files, as well as some other auxiliary files,
% will be deleted as soon as the output has been checked, unless
% \verb|TEST| is being run with the option \verb|-keep|.

% If most of the tests fail, it is likely that there's something
% \index{troubleshooting}\index{installation!troubleshooting}
% wrong with the installation. Look carefully through
% \verb|Makefile.config|, and consider turning down or even off
% optimization.

% If there are only a few tests that fail, and {\lsdalton} seems to exit
% normally in each case, there may just be some issues with numerical
% accuracy. Different machines give slightly different results, and
% while we've tried to allow for some slack in the tests, it may be
% that your machine yields numbers just outside the intervals we've
% specified as acceptable. A closer comparison of the results with
% numbers in the test script and/or the reference output files should
% reveal whether this is actually the case. If numerical (in)accuracy is
% the culprit, feel free to send your output file(s) to
% \verb|dalton-admin@kjemi.uio.no| so that we can adjust the numerical
% intervals accordingly.

% \section{Installing the program using CMake}\label{sec:CMake}
% \index{CMake}
% \index{installation!CMake}
% \index{installation!CMake}

% The program should be easily installed through the use of the supplied
% \verb|setup| script\index{configure script}. Based on the
% automatic detection of the architecture, the
% script will try to construct a CMake command on the
% basis of what kind of mathematical libraries are found, as well as user input, and run cmake. 
% The setup script takes a number of optional parameters like the following examples:
% \begin{verbatim}
% > ./setup --auto
% > ./setup --fc=gfortran --cc=gcc
% > ./setup --fc=ifort --cc=icc
% > ./setup --fc=gfortran --debug
% > ./setup --fc=ifort --cc=icc --debug --check --build=build-ifort-debug 
% --scratch=/path/to/scratch/dir
% > ./setup --fc=ifort --cc=icc --math-dir=/usr/local/lib/atlas
% > ./setup --fc=pgf90 --cc=pgc --internal-math
% > ./setup --fc=ifort --cc=icc --build=build-dir-name
% > ./setup --fc=mpif90 --cc=mpicc --mpi
% > ./setup --fc=ifort --cc=icc --profile
% > ./setup --fc=ifort --cc=icc --omp --math="-L/opt/intel/mkl/10.2.6.038/lib/32 
% -lmkl_lapack -lmkl_intel -lmkl_intel_thread -lmkl_core -lguide -lpthread"
% \end{verbatim}
% We strongly encourage that the environmental variable MATH$\_$ROOT is defined
% \begin{verbatim}
% > export MATH$\_$ROOT=/path/to/math/library
% \end{verbatim}
% The CMake configuration script will then automatically find the math library. 

% Do not use full path MATH$\_$ROOT='/opt/intel/mkl/lib/ia32'. CMake will append the
% correct paths depending on the processor and the default integer type.  If the
% MKL libraries that you want to use reside in
% /opt/intel/mkl/10.0.3.020/lib/em64t, then MATH$\_$ROOT is defined as::
% \begin{verbatim}
% > export MATH$\_$ROOT=/opt/intel/mkl/10.0.3.020
% \end{verbatim}

% the optional parameters are described here:
% \begin{description}
% \item \verb| --auto|     The setup script will automatically find a suitable Fortran and C compiler
% \item   \verb| --fc=fortrancompiler|     The user supplies the fortran compiler\\
%  options: $\{ $\verb| ifort , gfortran, pgf90, mpif90, g95, xlf, f90, open64 | $\}$
% \item   \verb| --cc=Ccompiler|     The user supplies the C compiler\\
%  options: $\{$ \verb| icc , gcc, pgcc, mpicc, xlc, open64 | $\}$
% \item  \verb| --debug| The Code is compiled using debug options
% \item  \verb| --check| The Code is compiled using check options
% \item   \verb| --mpiexec=/path/to/mpiexec|     The user supplies the mpirun or mpiexec the code should use when running test cases and the lsdalton script
% \item   \verb| --mathdir=/path/to/math|     The user supplies the math directory
% \item   \verb| --internal-math|     The code is compiled using the math lib that is part of {\lsdalton}. This option is highly discourage if efficiency is required. 
% \item   \verb| --mpi|     The code is compiled using Message Passing Interface (MPI) 
% \item   \verb| --mpi32|     The code is linked to an 32 bit integer version MPI library 
% \item   \verb| --omp|     The code is compiled using OpenMP
% \item   \verb| --mklflag|     The code is linked to MKL
% \item   \verb| --lseek|     adds the HAVE\verb|\_|NO\verb|\_|LSEEK64 precompiler flag
% \item   \verb| --int64|     The code is compiled using 64 bit integers
% \item   \verb| --csr|     The MKL compressed sparse row (CSR)feature are enabled
% \item   \verb| --scalapack|     The MKL SCALAPACK feature are enabled
% \item   \verb| --timings|     The timings of individual steps are printed at the bottom
% \item   \verb| --show|     shows the cmake command and exits
% \item   \verb| --scratch=/path/to/scratchdir|     The user supplies the scratch directory
% \item   \verb| --build=build-dir-name|     The user supplies the name of the build dir
% \item   \verb| --basis=/path/to/basisdir|     The user supplies the path to the basis set dir
% \item   \verb| --math="math string"|     explicit linker specification for math library
% \item  \verb| --D| Advanced option to forward options directly to cmake
% \end{description}

% The Output of the setup script should print a number of things including:
% \begin{description}
% \item \verb| System |  The computer architecture
% \item \verb| SCRATCH_DIR |  The scratch directory
% \item \verb| BASIS_DIR |  The scratch directory
% \item \verb| Fortran compiler |  The Fortran compiler
% \item \verb| C compiler |  The C compiler
% \item Whether CMake found a math lib or uses the lsdalton internal math lib 
% \item location of the build files - location of the build directory
% \end{description}

% to compile with the configured parameters (recommended) simply go to the 
% build directory and type 
% \begin{verbatim}
% > make
% \end{verbatim}

% To check that {\lsdalton} has been successfully installed, a fairly
% elaborate automatic test suite is provided in the distribution. Simply type
% \begin{verbatim}
% > make test
% \end{verbatim}
% A test script and all the test jobs and reference output files can be found in
% the \verb|.../lsdalton/test| directory. 
% Depending on your hardware, and build options this usually takes 10 min --- 2 hours.

