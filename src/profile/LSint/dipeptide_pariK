#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > dipeptide_pariK.info <<'%EOF%'
   dipeptide_pariK
   -------------
   Molecule:         C60/6-31G
   Wave Function:    HF
   Profile:          Exchange Matrix
   CPU Time:         9 min 30 seconds
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > dipeptide_pariK.mol <<'%EOF%'
BASIS
cc-pVTZ Aux=cc-pVTZdenfit
Dipeptide
EXCITATION DIAGNOSTIC
Atomtypes=4 Nosymmetry
Charge=6.0 Atoms=5
C         4.0014576502           -1.7731438622            0.0000000000
C         4.7903545875           -4.5228709584            0.0000000000
C         0.4598902846            1.1554627195            0.0000000000
C        -2.4123948835            0.9850878491            0.0000000000
C        -6.3863412584            3.3196850494            0.0000000000
Charge=8.0 Atoms=2
O         5.5211589635           -0.0000022372            0.0000000000
O        -3.5400868152           -1.0619994720            0.0000000000
Charge=7.0 Atoms=2
N         1.4622790902           -1.3693485029            0.0000000000
N        -3.6427586886            3.2308801492            0.0000000000
Charge=1.0 Atoms=10
H         0.1672102760           -2.7832943518            0.0000000000
H         4.0612384597           -5.4946066373            1.6724245908
H         4.0612384597           -5.4946066373           -1.6724245908
H         6.8470866062           -4.6124843532            0.0000000000
H         1.1149611609            2.2110510723            1.6619502748
H         1.1149611609            2.2110510723           -1.6619502748
H        -2.6410334961            4.8570903271            0.0000000000
H        -7.1427556233            2.3772819147            1.6734513085
H        -7.1427556233            2.3772819147           -1.6734513085
H        -6.9780425451            5.2927873898            0.0000000000

%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > dipeptide_pariK.dal <<'%EOF%'
**PROFILE
.EXCHANGE
**INTEGRALS
.PARI-K
**WAVE FUNCTIONS
.HF
*DENSOPT
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >dipeptide_pariK.check
cat >> dipeptide_pariK.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "Exchange energy, mat_dotproduct\(D,K\)\= * \-XXX\.XXXXXX" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

PASSED=1
for i in 1
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo PROF ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
