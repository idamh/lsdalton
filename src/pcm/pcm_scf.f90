!> @file
!> LSDALTON-side interface routines for the Polarizable Continuum Model
module pcm_scf

use iso_c_binding
use typedeftype, only: lssetting
use pcm_config, only: pcmtype, pcm_cfg, PCM, pcm_global, context_
use pcm_precision
use pcmsolver
use integralinterfacemod, only: II_get_ep_ab
use matrix_module
use lstiming, only: lstimer
use pcm_integrals, only: get_nuclear_mep, get_electronic_mep, get_mep
use pcm_write, only: pcm_write_file, pcm_write_file_separate

implicit none

public pcm_scf_driver
public get_pcm_energy

private

real(c_double)               :: pcm_energy = 0.0_dp
! A counter for the number of SCF iterations
integer(kind=regint_k), save :: scf_iteration_counter = 1

character(1)  :: mep_tot(7) = (/'T','o','t','M','E','P',char(0)/)
character(1)  :: asc_tot(7) = (/'T','o','t','A','S','C',char(0)/)
character(1)  :: mep_nuc(7) = (/'N','u','c','M','E','P',char(0)/)
character(1)  :: asc_nuc(7) = (/'N','u','c','A','S','C',char(0)/)
character(1)  :: mep_ele(7) = (/'E','l','e','M','E','P',char(0)/)
character(1)  :: asc_ele(7) = (/'E','l','e','A','S','C',char(0)/)

contains

!> \brief driver for PCM contributions in SCF
!> \author R. Di Remigio
!> \date 2015
!> \param density_matrix the current iteration density matrix
!> \param fock_matrix NxN matrix to allocate PCM Fock matrix contribution
!> \param pol_ene the polarization energy
!>
!> This subroutine retrieves the uncontracted potentials at cavity points
!> \latexonly $V_{\mu\nu}^I$ \endlatexonly and contracts them with the
!> apparent surface charge specified by the charge_name variable.
subroutine pcm_scf_driver(density_matrix, fock_matrix, pol_ene)

   type(matrix),      intent(in) :: density_matrix
   type(matrix),   intent(inout) :: fock_matrix
   real(c_double), intent(inout) :: pol_ene

   real(c_double), allocatable :: asc(:)
   real(kind=dp) :: ts_mep, te_mep, ts_pol, te_pol
   real(kind=dp) :: ts_fock, te_fock

   ! Compute MEP and ASC
   call lstimer('START ', ts_mep, te_mep, pcm_global%global_print_unit)
   call compute_mep_asc(density_matrix)
   call lstimer('MEPASC', ts_mep, te_mep, pcm_global%global_print_unit)

   ! pcm_energy is the polarization energy:
   ! U_pol = 0.5 * (U_NN + U_Ne + U_eN + U_ee)
   call lstimer('START ', ts_pol, te_pol, pcm_global%global_print_unit)
   pol_ene = pcmsolver_compute_polarization_energy(context_, mep_tot, asc_tot)
   call lstimer('PolEne', ts_pol, te_pol, pcm_global%global_print_unit)

   ! Now make the value of the polarization energy known throughout the module
   pcm_energy = pol_ene

   call lstimer('START ', ts_fock, te_fock, pcm_global%global_print_unit)
   allocate(asc(pcm_global%nr_points))
   asc = 0.0_dp
   call pcmsolver_get_surface_function(context_, pcm_global%nr_points, asc, asc_tot)
   call II_get_ep_ab(pcm_global%global_print_unit, &
                     pcm_global%global_error_unit, &
                     pcm_global%integral_settings, &
                     fock_matrix,                  &
                     int(pcm_global%nr_points),    &
                     pcm_global%tess_cent,         &
                     asc)
   deallocate(asc)
   call lstimer('PCMFCK', ts_fock, te_fock, pcm_global%global_print_unit)

   scf_iteration_counter = scf_iteration_counter + 1

end subroutine pcm_scf_driver

!> \brief handle to the PCM polarization energy
!> \author R. Di Remigio
!> \date 2014
!>
!> Retrieve the PCM polarization energy contribution.
real(c_double) function get_pcm_energy()

   get_pcm_energy = pcm_energy

end function get_pcm_energy

!> \brief driver for MEP and ASC calculation
!> \author R. Di Remigio
!> \date 2014
!> \param density_matrix the current iteration density matrix
!>
!> Calculate the molecular electrostatic potential and
!> the apparent surface charge at the cavity points.
!>
!> The user can control via the LSDALTON input the following:
!>    * switch between separate and total evaluation of the
!>      nuclear and electronic parts;
subroutine compute_mep_asc(density_matrix)

   type(matrix),    intent(in) :: density_matrix

   ! Local variables
   real(c_double), allocatable :: mep(:)
   real(c_double), allocatable :: asc(:)
   real(c_double), allocatable :: nuc_pot(:), nuc_pol_chg(:)
   real(c_double), allocatable :: ele_pot(:), ele_pol_chg(:)
   integer(4) :: i, irrep
   real(8) :: ts_meptot, te_meptot, ts_asctot, te_asctot
   real(8) :: ts_mepele, te_mepele, ts_ascele, te_ascele
   real(8) :: ts_mepnuc, te_mepnuc, ts_ascnuc, te_ascnuc
   real(8) :: tot_nuc_chg, tot_ele_chg, tot_chg

   allocate(mep(pcm_global%nr_points))
   mep = 0.0_dp
   allocate(asc(pcm_global%nr_points))
   asc = 0.0_dp
   ! The totally symmetric irrep
   irrep = 0

   SeparateMEPandASC: if (.not.(pcm_cfg%separate)) then
      call lstimer('START ', ts_meptot, te_meptot, pcm_global%global_print_unit)
      ! Calculate the (total) Molecular Electrostatic Potential
      call get_mep(int(pcm_global%nr_points), pcm_global%tess_cent, mep, density_matrix, &
                   pcm_global%integral_settings, pcm_global%global_print_unit, pcm_global%global_error_unit)
      call lstimer('MEPtot', ts_meptot, te_meptot, pcm_global%global_print_unit)

      call lstimer('START ', ts_asctot, te_asctot, pcm_global%global_print_unit)
      ! Set a cavity surface function with the MEP
      call pcmsolver_set_surface_function(context_, pcm_global%nr_points, mep, mep_tot)
      ! Compute polarization charges and set the proper surface function
      call pcmsolver_compute_asc(context_, mep_tot, asc_tot, irrep)
      ! Get polarization charges @tesserae centers
      call pcmsolver_get_surface_function(context_, pcm_global%nr_points, asc, asc_tot)
      call lstimer('ASCtot', ts_asctot, te_asctot, pcm_global%global_print_unit)

      ! Print some information
      PrintoutTotal: if (pcm_cfg%print_level > 5) then
         write(pcm_global%global_print_unit, '(20X, A, 6X, I6)') "MEP and ASC at iteration", scf_iteration_counter
         write(pcm_global%global_print_unit, '(A, T27, A, T62, A)') "Finite element #", "Total MEP", "Total ASC"
         tot_chg = 0.0_dp
         do i = 1, pcm_global%nr_points
           tot_chg = tot_chg + asc(i)
           write(pcm_global%global_print_unit, '(I6, 2(20X, F20.12))') i, mep(i), asc(i)
         end do
         write(pcm_global%global_print_unit, '(A, F20.12)') 'Sum of apparent surface charges ', tot_chg
         ! Write to file MEP and ASC
         call pcm_write_file(int(pcm_global%nr_points), mep, asc)
      end if PrintoutTotal

   else SeparateMEPandASC
      ! Allocation
      allocate(nuc_pot(pcm_global%nr_points))
      nuc_pot = 0.0_dp
      allocate(nuc_pol_chg(pcm_global%nr_points))
      nuc_pol_chg = 0.0_dp
      allocate(ele_pot(pcm_global%nr_points))
      ele_pot = 0.0_dp
      allocate(ele_pol_chg(pcm_global%nr_points))
      ele_pol_chg = 0.0_dp

      call lstimer('START ', ts_mepnuc, te_mepnuc, pcm_global%global_print_unit)
      call get_nuclear_mep(int(pcm_global%nr_points), pcm_global%tess_cent, nuc_pot)
      call lstimer('MEPnuc', ts_mepnuc, te_mepnuc, pcm_global%global_print_unit)

      call lstimer('START ', ts_ascnuc, te_ascnuc, pcm_global%global_print_unit)
      call pcmsolver_set_surface_function(context_, pcm_global%nr_points, nuc_pot, mep_nuc)
      call pcmsolver_compute_asc(context_, mep_nuc, asc_nuc, irrep)
      call pcmsolver_get_surface_function(context_, pcm_global%nr_points, nuc_pol_chg, asc_nuc)
      call lstimer('ASCnuc', ts_ascnuc, te_ascnuc, pcm_global%global_print_unit)

      call lstimer('START ', ts_mepele, te_mepele, pcm_global%global_print_unit)
      call get_electronic_mep(int(pcm_global%nr_points), pcm_global%tess_cent, ele_pot, density_matrix, &
                              pcm_global%integral_settings, pcm_global%global_print_unit, pcm_global%global_error_unit)
      call lstimer('MEPele', ts_mepele, te_mepele, pcm_global%global_print_unit)

      call lstimer('START ', ts_ascele, te_ascele, pcm_global%global_print_unit)
      call pcmsolver_set_surface_function(context_, pcm_global%nr_points, ele_pot, mep_ele)
      call pcmsolver_compute_asc(context_, mep_ele, asc_ele, irrep)
      call pcmsolver_get_surface_function(context_, pcm_global%nr_points, ele_pol_chg, asc_ele)
      call lstimer('ASCele', ts_ascele, te_ascele, pcm_global%global_print_unit)

      ! Print some information
      PrintoutSeparate: if (pcm_cfg%print_level > 5) then
         write(pcm_global%global_print_unit, '(60X, A, 6X, I6)') "MEP and ASC at iteration", scf_iteration_counter
         write(pcm_global%global_print_unit, '(A, T27, A, T62, A, T97, A, T132, A)') "Finite element #", &
         "Nuclear MEP", "Nuclear ASC", "Electronic MEP", "Electronic ASC"
         tot_nuc_chg = 0.0_dp
         tot_ele_chg = 0.0_dp
         do i = 1, pcm_global%nr_points
            tot_nuc_chg = tot_nuc_chg + nuc_pol_chg(i)
            tot_ele_chg = tot_ele_chg + ele_pol_chg(i)
           write(pcm_global%global_print_unit, '(I6, 4(20X, F20.12))') i, nuc_pot(i), nuc_pol_chg(i), ele_pot(i), ele_pol_chg(i)
         end do
         write(pcm_global%global_print_unit, '(A, F20.12)') 'Sum of nuclear apparent charges ', tot_nuc_chg
         write(pcm_global%global_print_unit, '(A, F20.12)') 'Sum of electronic apparent charges ', tot_ele_chg
         write(pcm_global%global_print_unit, '(A, F20.12)') 'Sum of apparent surface charges ', tot_nuc_chg + tot_ele_chg
         ! Write to file MEP and ASC
         call pcm_write_file_separate(int(pcm_global%nr_points), nuc_pot, nuc_pol_chg, ele_pot, ele_pol_chg)
      end if PrintoutSeparate

      ! Obtain vector of total MEP
      mep(:) = nuc_pot(:) + ele_pot(:)
      call pcmsolver_set_surface_function(context_, pcm_global%nr_points, mep, mep_tot)

      ! Obtain vector of total polarization charges
      asc(:) = nuc_pol_chg(:) + ele_pol_chg(:)
      call pcmsolver_set_surface_function(context_, pcm_global%nr_points, asc, asc_tot)

      deallocate(nuc_pot)
      deallocate(nuc_pol_chg)
      deallocate(ele_pot)
      deallocate(ele_pol_chg)
   end if SeparateMEPandASC

   deallocate(mep)
   deallocate(asc)

end subroutine compute_mep_asc

end module
