!> @file

! TODO All threshh should be input!
!
!

module wannier_ao_integrals
  use precision
  use wannier_utils !, only:
  use wannier_types, only: &
       bravais_lattice
  use matrix_operations, only: &
       mat_init, &
       mat_abs_max_elm, &
       mat_print, &
       mat_daxpy
  !use wannier_fmm, only: wannier_multipole_expan
  use typedef, only: &
       typedef_setmolecules, &
       print_moleculeinfo
  use molecule_type, only: &
       init_moleculeinfo, &
       copy_molecule, &
       free_moleculeinfo
  use molecule_typetype, only: &
       moleculeinfo
  use integralinterfacemod, only: &
       ii_get_overlap, &
       ii_get_kinetic, &
       ii_get_nucel_mat, &
       ii_get_nucpot
  use typedeftype, only: &
       lssetting
  use wlattice_domains, only: &
       ltdom_init, &
       ltdom_free, &
       ltdom_getlayer, &
       lattice_domains_type, &
       ltdom_getdomain
  use lattice_storage, only: &
       lmatrixp, &
       lts_init, &
       lts_free, &
       lts_get, &
       lts_newcutoff, &
       lts_mat_init, &
       lts_store, &
       lts_load, &
       lts_add, &
       linteger, &
       lmatrixp, &
       lts_print
  use matrix_module, only: &
       matrix
  use timings_module, only: &
       timings_start, &
       timings_stop, &
       timings_type
  use matrix_operations, only: &
       mat_zero, &
       mat_free, &
       mat_print
  use integralinterfacemod, only: &
       ii_get_maxgabelm_screenmat
#ifdef RUN_WANNIER_TESTS
  use wannier_test_module
#endif
  implicit none
  private
  public :: wcalc_nucpot, wcalc_nucattrc, wcalc_tmat, w_init_maxgab, wcalc_smat, wcalc_oneptoper, wcalc_Z_nearfield
contains

  ! TODO unnecc much mem loaded
  !> @brief Calculate the 1-pt operators T + Z
  !> @author Karl R. Leikanger
  !> @date 2015
  !> @param
  !> @param fockmat_cutoff Size of the Fock matrix in terms of cells in each direction
  function wcalc_oneptoper(lsset, maxgab, refcell, blat, nbast, timings, nfcutoffs, &
       & screening_thr, fockmat_cutoff, debug_level, no_redundant, lupri, luerr)

    integer, intent(in) :: nbast, fockmat_cutoff(3), nfcutoffs(3), debug_level, lupri, luerr
    type(moleculeinfo), intent(in) :: refcell
    type(linteger), intent(in) :: maxgab
    type(lssetting), intent(inout) :: lsset
    type(bravais_lattice), intent(in) :: blat
    type(timings_type), intent(inout) :: timings
    type(lmatrixp) :: wcalc_oneptoper
    real(realk), intent(in) :: screening_thr
    type(lmatrixp) :: tmat, zmat
	 logical, intent(in) :: no_redundant 

    !The cutoffs for the T matrix are set automatically in order to include all significant
    !contributions
    tmat = wcalc_tmat( &
         & lsset, refcell, blat, nbast, timings, screening_thr, lupri, luerr)  

    !The cutoffs for the Z matrix are set automatically in order to include all significant
    !contributions
    !zmat = wcalc_nucattrc(lsset, maxgab, refcell, &
    !     & blat, nbast, timings, nfcutoffs, screening_thr, lupri, luerr)

    zmat = wcalc_Z_nearfield(lsset, maxgab, refcell, &
         & blat, nbast, timings, nfcutoffs, fockmat_cutoff, screening_thr, no_redundant, &
			& lupri, luerr)
    
    if (debug_level > 5) then
       write(luerr,*) 'Kinetic matrix'
       call lts_print(tmat,luerr,1)
       
       write(luerr,*) 'Vne matrix'
       call lts_print(zmat,luerr,1)
    endif
       
    !Returns the sum of the Z and T matrix with the cutoffs set as the max of the two
    wcalc_oneptoper = lts_add(tmat, 1.0_realk, zmat, 1.0_realk)
    call lts_free(zmat)
    call lts_free(tmat)

    !Redimension the matrix in order to fit the cutoffs of the Fock matrix
    call lts_newcutoff(wcalc_oneptoper, fockmat_cutoff)  
    
  end function wcalc_oneptoper

  !> @author Johannes Rekkedal
  !> @author Karl Leiaknger
  !> @date 2013
  !> @brief For computing the near-field Nuc. repulsion energy.
  !> @param
  !> @param
  !> @param
  !> @param lupri Logical print unit output file.
  !> @param luerr Logical print unit error file.
  !> @param lsset Integral settings.
  function wcalc_nucpot(lsset, refcell, blat, nfcutoffs, timings, &
       & lupri, luerr) result(enuc)
    integer, intent(in) :: lupri, luerr, nfcutoffs(3)
    type(moleculeinfo), intent(in) :: refcell
    type(bravais_lattice), intent(in) :: blat
    type(lssetting), intent(inout) :: lsset
    type(timings_type), intent(inout) :: timings
    real(realk) :: enuc

    real(realk) :: enuctmp, transl(3)
    integer :: i, indx
    type(lattice_domains_type) :: dom
    type(moleculeinfo) :: moltmp

    enuc = 0.0_realk
    write (lupri, *) 'Calculating near-field  nuclear - nuclear repulsion energy...'
    call timings_start(timings, 'Enuc')

    
    call copy_molecule(refcell, moltmp, lupri)
    call ltdom_init(dom, blat%dims, maxval(nfcutoffs))

    call ltdom_getdomain(dom, nfcutoffs, incl_redundant=.true.) !TODO incl_redundant?
    do i = 1, dom%nelms; indx = dom%indcs(i)

       transl =  wu_indxtolatvec(blat, indx)
       call wcopy_atompositions(moltmp, refcell)
       call wtranslate_molecule(moltmp,transl)
       call typedef_setmolecules(lsset, refcell, 1, moltmp, 3)

       if (indx == 0) then
          call II_get_nucpot(lupri, luerr, lsset, enuctmp)
       else
          call wget_nucpot(lupri, luerr, lsset, enuctmp)
       endif
       enuc = enuc + enuctmp
    enddo

    call timings_stop(timings, 'Enuc')
    call ltdom_free(dom)
    call free_moleculeinfo(moltmp)
  end function wcalc_nucpot

  !> \brief calculates the nuclear repulsion energy contribution between two cells
  !> \author s. reine and t. kjaergaard
  !> \modified by j. rekkedal
  !> \date 2012
  !> \param lupri default print unit
  !> \param luerr default error print unit
  !> \param setting integral evalualtion settings
  !> \param nucpot the nuclear repulsion energy contribution
  subroutine wget_nucpot(lupri, luerr, setting, nucpot)
    implicit none
    type(lssetting) :: setting
    integer :: lupri, luerr
    real(realk) :: nucpot
    integer :: i, j
    real(realk) :: pq(3),distance
    logical :: nobqbq

    !call time_ii_operations1()
    nobqbq = setting%scheme%nobqbq
    nucpot = 0.0e0_realk

    do i = 1, setting%molecule(1)%p%natoms
       if(setting%molecule(1)%p%atom(i)%phantom) cycle

       do j= 1, setting%molecule(1)%p%natoms
          if (setting%molecule(1)%p%atom(j)%phantom) cycle

          if (setting%molecule(1)%p%atom(i)%pointcharge &
               & .and. setting%molecule(1)%p%atom(j)%pointcharge &
               & .and. nobqbq) cycle

          pq(1:3) = setting%molecule(1)%p%atom(i)%center(1:3) &
               & - setting%molecule(3)%p%atom(j)%center(1:3)

          distance = sqrt(pq(1)*pq(1) + pq(2)*pq(2) + pq(3)*pq(3))
          nucpot = nucpot + setting%molecule(1)%p%atom(i)%charge &
               & * setting%molecule(3)%p%atom(j)%charge / distance
       enddo
    enddo
    nucpot=nucpot/2.
    !call time_ii_operations2(job_ii_get_nucpot) !TODO activate?

  end subroutine wget_nucpot

  !> @brief Initialise and compute the Z-matrix with the nearfield cutoffs
  !> @author Elisa Rebolini
  !> @date April 2016
  !> @param
  function wcalc_Z_nearfield(lsset, maxgab, refcell, blat, nbast, timings, nfcutoffs, &
       fockmat_cutoffs, screening_thr, no_redundant, lupri, luerr) result(zmat)
    
    integer, intent(in)               :: nbast, nfcutoffs(3), fockmat_cutoffs(3), lupri, luerr
    type(moleculeinfo), intent(in)    :: refcell
    type(bravais_lattice), intent(in) :: blat
    type(linteger), intent(in)        :: maxgab
    type(lssetting), intent(inout)    :: lsset
    type(timings_type), intent(inout) :: timings
    type(lmatrixp)                    :: zmat
    real(realk), intent(in)           :: screening_thr
    logical, intent(in)               :: no_redundant

    type(lattice_domains_type) :: dom_F, dom_nf
    integer                    :: l, indxl, m ,indxM
    real(realk)                :: transL(3), transM(3)
    type(matrix),pointer       :: zmat_0L
    type(matrix)               :: zmat_tmp, h1
    type(moleculeinfo)         :: molL, molM
    
    !Initialisation of the Z matrix with the Fock matrix cutoffs
    !Redundancy = .false. so that only the block Z_0,L with L>=0 are computed
    call lts_init(zmat, blat%dims, fockmat_cutoffs, nbast, nbast, no_redundant) 

    !Initialisation of the domains
    call ltdom_init(dom_F, blat%dims, maxval(fockmat_cutoffs))
    call ltdom_init(dom_nf, blat%dims, maxval(nfcutoffs))
    call ltdom_getdomain(dom_nf, nfcutoffs, incl_redundant = .true.)
    call ltdom_getdomain(dom_F, fockmat_cutoffs, incl_redundant = no_redundant)

    !Copy reference cell for integral call
    call copy_molecule(refcell, molL, lupri)
    call copy_molecule(refcell, molM, lupri)

    !Initialisation of tmp matrices
    call mat_init(zmat_tmp, nbast, nbast)
    call mat_init(h1, nbast, nbast)
    
    loopl: do l = 1, dom_F%nelms; indxL = dom_F%indcs(l)
       ! get Zmat of cell indxL
       call lts_mat_init(zmat, indxL)
       zmat_0L => lts_get(zmat, indxL)
       call mat_zero(zmat_0L)
       !Translate cell L
       call wcopy_atompositions(molL, refcell)
       transL =  wu_indxtolatvec(blat, indxL)
       call wtranslate_molecule(molL,transL)

       call mat_zero(zmat_tmp)
       loopm: do m = 1, dom_nf%nelms; indxM = dom_nf%indcs(m)
          !debug
          !if (abs(indxM) /= maxval(nfcutoffs)) cycle
          !debug
          call wcopy_atompositions(molM, refcell)
          transM = wu_indxtolatvec(blat, indxM)
          call wtranslate_molecule(molM, transM)
          call typedef_setmolecules(lsset, refcell, 1, molL, 2, molM, 3)
          call II_get_nucel_mat(lupri, luerr, lsset, H1)
          call mat_daxpy(1._realk, H1, zmat_tmp)
       enddo loopm
       call mat_daxpy(1._realk, zmat_tmp, zmat_0L)
    enddo loopl
    
    !Free the domains
    call ltdom_free(dom_F)
    call ltdom_free(dom_nf)

    !Free moleculeinfo
    call free_moleculeinfo(molL)
    call free_moleculeinfo(molM)

    !Free tmp matrices
    call mat_free(zmat_tmp)
    call mat_free(h1)
  end function wcalc_Z_nearfield
  
  !> @brief Init and calculate z-matrix. The cutoffs is automatically set to 
  !> include all non-negligible contributions according to the cutoff thrsh.
  !> @param
  !> @author Karl Leikanger
  !> @date 2015
  function wcalc_nucattrc(lsset, maxgab, refcell, blat, nbast, timings, nfcutoffs, &
       & screening_thr, lupri, luerr) result(zmat)

    integer, intent(in) :: nbast, nfcutoffs(3), lupri, luerr
    type(moleculeinfo), intent(in) :: refcell
    type(bravais_lattice), intent(in) :: blat
    type(linteger), intent(in) :: maxgab
    type(lssetting), intent(inout) :: lsset
    type(timings_type), intent(inout) :: timings
    type(lmatrixp) :: zmat
    real(realk), intent(in) :: screening_thr

    real(realk) :: maxelm_layer, maxelm, trans1(3), trans2(3)
    integer :: i, j, indx1, indx2, layer, newcutoffs(3), coor(3)
    integer, pointer :: gabtmp
    type(lattice_domains_type) :: dom1, dom2
    type(matrix) :: tmp1, h1
    type(moleculeinfo) :: moltmp1, moltmp2
    type(matrix), pointer :: zmat_pt

    real(realk) :: thrsh
    thrsh = int(log(screening_thr))

    write (lupri, *) 'Calculating Z - matrix (nucl. attr.) ...'
    call timings_start(timings, 'Z mat')

    !call set_lstime_print(.false.)

    call mat_init(tmp1, nbast, nbast)
    call mat_init(h1, nbast, nbast)

    i = maxval(nfcutoffs) ! todo [i ,i ,i] -> nfcutoffs
    call lts_init(zmat, blat%dims, nfcutoffs, nbast, nbast, .false.) 
    call ltdom_init(dom1, blat%dims, maxval(nfcutoffs))
    call ltdom_init(dom2, blat%dims, maxval(nfcutoffs))
    call copy_molecule(refcell, moltmp1, lupri)
    call copy_molecule(refcell, moltmp2, lupri)

    newcutoffs =  0

    layers: do layer = 0, maxval(nfcutoffs) !TODO loop over all cells??
       maxelm_layer = 0.0_realk
       maxelm = 0.0_realk
       call ltdom_getlayer(dom1, layer, incl_redundant=.false.) !only non - redundant
       do i = 1, dom1%nelms; indx1 = dom1%indcs(i)

          gabtmp => lts_get(maxgab, indx1) 
          if (.not. associated(gabtmp)) cycle
          if (gabtmp < thrsh) cycle

          ! get tmat of cell indx
          call lts_mat_init(zmat, indx1)
          zmat_pt => lts_get(zmat, indx1)
          call mat_zero(zmat_pt)

          ! get translated refcell indx1
          call wcopy_atompositions(moltmp1, refcell)
          trans1 = wu_indxtolatvec(blat, indx1)
          call wtranslate_molecule(moltmp1, trans1)

          call mat_zero(tmp1)
          call ltdom_getdomain(dom2, nfcutoffs, incl_redundant=.true.) !include redundant
          do j = 1, dom2%nelms; indx2 = dom2%indcs(j)

             ! get translated refcell indx2
             call wcopy_atompositions(moltmp2, refcell)
             trans2 = wu_indxtolatvec(blat, indx2)
             call wtranslate_molecule(moltmp2, trans2)
             call typedef_setmolecules(lsset, refcell, 1, moltmp1, &
                  & 2, moltmp2, 3)

             call II_get_nucel_mat(lupri, luerr, lsset, H1)
             call mat_daxpy(1._realk, H1, tmp1)
          enddo

          call mat_daxpy(1._realk, tmp1, zmat_pt)

          call mat_abs_max_elm(tmp1, maxelm)
          maxelm_layer = max(maxelm_layer, maxelm)
          if (maxelm > 10**thrsh) then
             coor = abs(wu_indxtocoor(indx1))
             where (newcutoffs < coor) newcutoffs = coor
          endif
       enddo
       if (maxelm_layer < 10**thrsh) exit layers
    enddo layers

    !shrink tmat to smallest necc size (in layers)
    call lts_newcutoff(zmat, newcutoffs)

    call mat_free(h1)	
    call mat_free(tmp1)	
    call timings_stop(timings, 'Z mat')

    write (lupri, *) ' ----- Z - matrix calculated ----- ' 
    write (lupri, *) 'Smallest element cutoff threshold for layers =', thrsh
    write (lupri, *) 'Cutoff in layers (Lx, Ly, Lz) =', newcutoffs

    call ltdom_free(dom1)
    call ltdom_free(dom2)
    call free_moleculeinfo(moltmp1)
    call free_moleculeinfo(moltmp2)

    ! call set_lstime_print(.true.)
  end function wcalc_nucattrc

  !> @brief Init and calculate t-matrix. The cutoffs is automatically set to 
  !> include all non-negligible contributions according to the cutoff thrsh.
  !> @author K Leikanger
  !> @date 2015
  function wcalc_tmat(lsset, refcell, blat, nbast, timings, screening_thr, &
       & lupri, luerr) result(tmat)
    !use wannier_parameters, only: TTRESH
    type(lssetting), intent(inout) :: lsset
    type(moleculeinfo), intent(in) :: refcell
    type(bravais_lattice), intent(in) :: blat
    integer, intent(in) :: nbast, lupri, luerr
    type(lmatrixp) :: tmat
    type(timings_type), intent(inout) :: timings
    real(realk), intent(in) :: screening_thr

    integer :: cutoffs(3), newcutoffs(3), coor(3), layer, indx, i
    type(lattice_domains_type) :: dom
    logical :: iterate
    type(moleculeinfo) :: moltmp
    type(matrix), pointer :: tmat_tmp
    real(realk) :: maxelm, trans(3)

    write (lupri, *) 'Calculating T - matrix...'
    call timings_start(timings, 'T mat')

    cutoffs = 5
    newcutoffs = 0

    call lts_init(tmat, blat%dims, cutoffs, nbast, nbast, .false.)
    call ltdom_init(dom, blat%dims, maxval(cutoffs))
    call copy_molecule(refcell, moltmp, lupri)

    layer = 0
    iterate = .true.
    loop: do while (iterate)
       iterate = .false.

       ! loop over all elements in the given layer.
       call ltdom_getlayer(dom, layer, incl_redundant=.false.)
       do i = 1, dom%nelms; indx = dom%indcs(i)
          ! get translated refcell
          call wcopy_atompositions(moltmp, refcell)
          trans =wu_indxtolatvec(blat, indx)
          call wtranslate_molecule(moltmp, trans)
          call typedef_setmolecules(lsset, refcell, 1, moltmp, 3)

          ! get tmat of indx
          call lts_mat_init(tmat, indx)
          tmat_tmp => lts_get(tmat, indx)
          !TODO joh only calculates this if maxgab is larger than some thr
          call ii_get_kinetic(lupri, luerr, lsset, tmat_tmp)

          ! find largest necc cutoff, decide wether to iter over the next layer
          call mat_abs_max_elm(tmat_tmp, maxelm)
          if (maxelm > screening_thr) then
             iterate = .true.
             coor = abs(wu_indxtocoor(indx))
             where (newcutoffs < coor) newcutoffs = coor
          endif
       enddo
       layer = layer + 1

       if ((layer == maxval(cutoffs)) .and. (iterate)) then 
          cutoffs = cutoffs + 5
          call lts_newcutoff(tmat, cutoffs)
          call ltdom_free(dom)
          call ltdom_init(dom, blat%dims, maxval(cutoffs))
       end if
    enddo loop

    !shrink tmat to smallest necc size (in layers)
    call lts_newcutoff(tmat, newcutoffs)

    call timings_stop(timings, 'T mat')
    write (lupri, *) ' ----- T - matrix calculated ----- ' 
    write (lupri, *) 'Smallest element cutoff threshold for layers =', screening_thr
    write (lupri, *) 'Cutoff in layers (Lx, Ly, Lz) =', newcutoffs

    call ltdom_free(dom)
    call free_moleculeinfo(moltmp)
  end function wcalc_tmat

  ! TODO clean up
  !
  !> @brief Get all maxgab > maxgab_thr lts_get(maxgab, i) is 
  !> max(log(sqrt(g_pqrs))) (?) where pq is in the refcell and rs is in cell i(?). 
  !> Maxgab is used for coul. integral screening. 
  !> After this s.r., maxgab%cutoffs is set s.t. all g_pqrs > maxgab_thr is included 
  !> in maxgab%cutoffs
  !> @author
  !> @date
  !> @param
  !> @param
  !> @param
  function w_init_maxgab(lsset, refcell, blat, nbast, timings, screening_thr, &
       & lupri, luerr)

    integer, intent(in) :: lupri, luerr, nbast
    type(lssetting), intent(inout) :: lsset
    type(moleculeinfo), intent(in) :: refcell
    type(bravais_lattice), intent(in) :: blat
    type(timings_type), intent(inout) :: timings
    real(realk), intent(in) :: screening_thr

    type(linteger) :: w_init_maxgab

    type(moleculeinfo), target :: moltmp 
    integer :: indx, i, j, layer, maxl, maxelm_layer, newcutoff(3), coor(3), &
         & maxgab_thr, cutoff0(3)
    type(lattice_domains_type) :: dom
    character (len=22) :: molname
    logical :: iterate
    integer, pointer :: maxgab_ptr
    integer(short) :: maxgabtmp
    type(linteger) :: maxgab
    real(realk) :: trans(3)

    write (lupri, *) 'Calculating max(log(sqrt G0l)) ...'
    call timings_start(timings, 'maxgab')

    maxgab_thr = int(log10(screening_thr))

    maxl = 10
    newcutoff = [0, 0, 0]
    cutoff0 = [maxl, maxl, maxl]
    call lts_init(maxgab, blat%dims, cutoff0, .true.)
    call ltdom_init(dom, blat%dims, maxl)
    call copy_molecule(refcell, moltmp, lupri)

    !call set_lstime_print(.false.)

    layer = 0 ! loop over layers
    loop: do 		
       iterate = .false.
       call ltdom_getlayer(dom, layer)
       do i = 1, dom%nelms; indx = dom%indcs(i)

          ! set up molecule for cell indx
          call wcopy_atompositions(moltmp, refcell)
          trans = wu_indxtolatvec(blat, indx)
          call wtranslate_molecule(moltmp, trans)
          call typedef_setmolecules( &
               & lsset, refcell, 1, moltmp, 2, refcell, 3, moltmp, 4)

          ! find maxgab for cell indx
          call ii_get_maxgabelm_screenmat( &
               & lupri, luerr, lsset, nbast, maxgabtmp)
          maxgab_ptr => lts_get(maxgab, indx)
          maxgab_ptr = maxgabtmp

          ! find the largest cutoffs where for all elms maxgab > maxgab_thr
          if (maxgab_thr < maxgab_ptr) then
             iterate = .true.
             coor = wu_indxtocoor(indx)
             do j = 1, 3
                if (abs(coor(j)) > newcutoff(j)) then
                   newcutoff(j) = abs(coor(j))
                endif
             enddo
          endif
       enddo

       if (.not. iterate) exit loop

       ! resize maxgab if layer = maxl
       if (layer == maxl) then
          maxl = maxl + 5
          call lts_newcutoff(maxgab, [maxl, maxl, maxl])
          call ltdom_free(dom)
          call ltdom_init(dom, blat%dims, maxl)
       endif
       layer = layer + 1
    enddo loop

    !call set_lstime_print(.true.)

    call lts_newcutoff(maxgab, newcutoff)
    call ltdom_free(dom)
    call free_moleculeinfo(moltmp)

    call timings_stop(timings, 'maxgab')
    write (lupri, *) ' ----- max(logsqrt(Gab)) - matrix calculated ----- ' 
    write (lupri, *) 'Cutoff threshold for layers =', maxgab_thr
    write (lupri, *) 'Cutoff in layers (Lx, Ly, Lz) =', newcutoff

    w_init_maxgab = maxgab

  end function w_init_maxgab

  !> @brief Compute the overlap matrix on the lattice (non-redundant)
  !> @author Karl R. Leikanger
  !> @date 2015
  !> @param lsset LSdalton settings
  !> @param refcell Molecular info on the reference cell
  !> @param blat Info on the Bravais lattice
  !> @param nbast Number of basis functions in the reference cell
  !> @param nlayers Number of layers
  !> @param timings 
  !> @param screening_thr
  !> @param lupri
  !> @param luerr
  function wcalc_smat(lsset, refcell, blat, nbast, nlayers, timings, screening_thr, lupri, luerr)

    integer, intent(in) :: lupri, luerr, nbast, nlayers
    real(realk), intent(in) :: screening_thr
    type(lssetting), intent(inout) :: lsset
    type(moleculeinfo), intent(in) :: refcell
    type(bravais_lattice), intent(in) :: blat
    type(lmatrixp) :: wcalc_smat
    type(timings_type), intent(inout) :: timings

    type(lmatrixp) :: smat
    type(moleculeinfo), target :: moltmp
    integer :: indx, i, j, coor(3), newcutoff(3), cutoff0(3), k, layer, maxl
    real(realk) :: maxelm, trans(3)
    type(lattice_domains_type) :: dom
    type(matrix), pointer :: smat_tmp
    logical :: iterate
    character (len=22) :: molname

    write (lupri, *) 'Calculating S - matrix...'
    call timings_start(timings, 'S mat')

    maxl = 2*nlayers+1
    newcutoff = [0, 0, 0]
    cutoff0 = [maxl, maxl, maxl]
    call lts_init(smat, blat%dims, cutoff0, nbast, nbast, .false.)
    call ltdom_init(dom, blat%dims, maxl)
    call copy_molecule(refcell, moltmp, lupri)

    ! Calc smat layer for layer. 
    layer = 0
    loop: do
       iterate = .false.
       ! loop over all elements in the given layer.
       call ltdom_getlayer(dom, layer, .false.)
       do j = 1, dom%nelms; indx = dom%indcs(j)
          if (indx == 0) then
             call typedef_setmolecules(lsset, refcell, 1, 3)
          else 
             call wcopy_atompositions(moltmp, refcell)
             trans = wu_indxtolatvec(blat, indx)
             call wtranslate_molecule(moltmp, trans)
             call typedef_setmolecules(lsset, refcell, 1, moltmp, 3)
          endif

          ! get smat of indx
          call lts_mat_init(smat, indx)
          smat_tmp => lts_get(smat, indx)
          call II_get_overlap(lupri, luerr, lsset, smat_tmp)

          ! find the largest cutoffs where for all elements > smat_thr
          call mat_abs_max_elm(smat_tmp, maxelm)
          if (screening_thr < maxelm) then
             iterate = .true.
             coor = wu_indxtocoor(indx)
             where (abs(coor) > newcutoff) newcutoff = abs(coor)
          endif
       enddo

       if (.not. iterate) exit loop

       ! resize s if layer = maxl
       if (layer == maxl) then
          maxl = maxl + 5
          call lts_newcutoff(smat, [maxl, maxl, maxl])
          call ltdom_free(dom)
          call ltdom_init(dom, blat%dims, maxl)
       endif

       layer = layer + 1
    enddo loop

    !shrink smat to smallest necessary size (in layers)
    call lts_newcutoff(smat, newcutoff)

    call timings_stop(timings, 'S mat')
    write (lupri, *) ' ----- S - matrix calculated ----- ' 
    write (lupri, *) 'Smallest element cutoff threshold for layers =', screening_thr 
    write (lupri, *) 'Cutoff in layers (Lx, Ly, Lz) =', newcutoff

    call ltdom_free(dom)
    call free_moleculeinfo(moltmp)
    wcalc_smat = smat 
  end function wcalc_smat

end module wannier_ao_integrals
