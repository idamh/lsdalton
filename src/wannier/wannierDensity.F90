module wannier_dens

  use precision
  use wannier_types, only: &
       wannier_config, &
       bravais_lattice
  use molecule_typetype, only: &
       moleculeinfo
  use typedeftype, only: &
       daltoninput, &
       lssetting
  use wlattice_domains, only: ltdom_init, &
       ltdom_free, &
       ltdom_getlayer, &
       lattice_domains_type, &
       ltdom_getdomain, &
       ltdom_getintersect, &
       ltdom_contains
  use lattice_storage
  use matrix_module, only: &
       matrix
  use timings_module
  use memory_handling, only: mem_alloc, mem_dealloc

  implicit none
  private
  public :: w_calc_dens
  
contains
  !> @brief Calculate density matrix from MO - coeff.
  !> D_0mu,Lnu = sum_M sum_i c_0mu,Mi c^T_Lnu,Mi.
  !> For dens we only store matrix-elem of cells where the cellindex
  !> is positive, but becauce of translational invariance + hermiticity these 
  !> are the transpose of the matrices with the negatice cellindx
  !> (neg transl. vec.)
  !> @author Karl R. Leikanger
  !> @date 2015
  !> @param mos MO coefficients
  !> @param blat Bravais lattice
  !> @param dens Density matrix on the lattice
  subroutine w_calc_dens(mos, blat, debug, dens, n_occ)

    type(lmatrixp), intent(in) :: mos
    type(bravais_lattice), intent(in) :: blat
    type(lmatrixp), intent(inout) :: dens
    integer, intent(in) :: n_occ
    logical :: debug

    type(lattice_domains_type) :: dom_l, dom_m
    integer :: i, j, indx_l, indx_m, n_mo, k
    type(matrix), pointer :: matpt_lpm, matpt_m, dens_pt
   
    n_mo = mos%nrow
    call lts_zero(dens)
    call ltdom_init(dom_l, blat%dims, 2*maxval(mos%cutoffs))
    call ltdom_init(dom_m, blat%dims, maxval(mos%cutoffs))

    call ltdom_getdomain(dom_l, dens%cutoffs, incl_redundant=debug)
    do i = 1, dom_l%nelms; indx_l = dom_l%indcs(i)
       dens_pt => lts_get(dens, indx_l)
       
!!$       call ltdom_getintersect( dom_m, &
!!$            & mos%cutoffs, wu_indxtocoor(-indx_l), & !????
!!$            & mos%cutoffs, [0, 0, 0], &
!!$            & incl_redundant=.true.)

       call ltdom_getdomain(dom_m, mos%cutoffs, incl_redundant=.true.)
       do j = 1, dom_m%nelms; indx_m = dom_m%indcs(j)
          ! C_0mu,Mi
          matpt_m => lts_get(mos, indx_m)
          if (.not. associated(matpt_m)) call lsquit('matpt_m',-1)
          ! may be at some point we will not allocate non - significant MO blocks

          ! C_0nu,M-Li
          matpt_lpm => lts_get(mos, -indx_l + indx_m)
          if (.not. associated(matpt_lpm)) cycle 
          ! may be at some point we will not allocate non - significant MO blocks

          !D_0mu,Lnu = sum_M sum_i c_0mu,Mi c^T_0nu,M-Li
          call dgemm('N', 'T', n_mo, n_mo, n_occ, 1.0_realk, &
               & matpt_m%elms, n_mo, &
               & matpt_lpm%elms, n_mo, &
               & 1.0_realk, dens_pt%elms, n_mo)

       enddo ! j loop
    enddo ! i loop

    call ltdom_free(dom_l)
    call ltdom_free(dom_m)
   
  end subroutine w_calc_dens

end module wannier_dens
