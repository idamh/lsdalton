!> @file WannierMOFock.F90
!>Computes the Wannier Fock matrix elements in MO basis
!> (without the orthogonalising potential)

module wannier_fockMO

  use precision
  use wannier_types, only: &
       wannier_config, &
       bravais_lattice
  use molecule_typetype, only: &
       moleculeinfo
  use molecule_type, only: &
       copy_molecule, &
       free_moleculeinfo
  use typedef!, only: &
       !typedef_setmolecules
  use typedeftype, only: &
       daltoninput, &
       lssetting
  use wlattice_domains, only: &
       ltdom_init, &
       ltdom_free, &
       ltdom_getlayer, &
       lattice_domains_type, &
       ltdom_getdomain, &
       ltdom_getintersect, &
       ltdom_contains
  use lattice_storage, only: &
       lmatrixp, &
       llmatrixp, &
       linteger, &
       lmatrixarrayp, &
       lts_mat_init, &
       lts_init, &
       lts_free, &
       lts_get, &
       lts_axpy, &
       lts_store, &
       lts_load, &
       lts_zero, &
       lts_print, &
       lts_copy
  use matrix_module, only: &
       matrix
  use timings_module
  use memory_handling, only: &
       mem_alloc, mem_dealloc
!!$  use wannier_fmm, only: &
!!$       pbc_mat_redefine_q, &
!!$       pbc_multipl_moment_matorder
!!$  use wannier_utils, only: &
!!$       wu_indxtocoor, &
!!$       wu_indxtolatvec, &
!!$       wtranslate_molecule, &
!!$       wcopy_atompositions
!!$  use wannier_test_module
  use matrix_operations, only: &
       mat_daxpy, &
       mat_free, &
       mat_init, &
       mat_mul, &
       mat_zero, &
       mat_dotproduct, &
       mat_abs_max_elm, &
       mat_trans, &
       mat_print
!!$  use integralinterfacemod, only: &
!!$       ii_get_coulomb_mat, &
!!$       ii_get_exchange_mat, &
!!$       II_get_nucel_mat
!!$  use configurationType
!!$  use integraloutput_type
!!$  use LSparameters
!!$  use ls_Integral_Interface

  implicit none
  private
  public :: w_calc_mo_l, w_calc_fock_mo, w_calc_orthpot_mo, w_get_maxODF, w_calc_AO2MO_shukla
  
contains

  subroutine w_calc_AO2MO_shukla(fockMO, fockmat, mos, wconf, lupri, luerr)
    type(lmatrixp), intent(inout)       :: fockmat, mos, fockMO
    type(wannier_config), intent(inout) :: wconf
    integer, intent(in)                 :: lupri, luerr

    type(lattice_domains_type)  :: dom
    integer                     :: l, indx_l
    type(matrix), pointer       :: FMO_0L_ptr
    
    !Set up the domain d0
    call ltdom_init(dom, mos%periodicdims, 2*maxval(mos%cutoffs))
    call ltdom_getdomain(dom, 2*mos%cutoffs, incl_redundant=.true.)

    call lts_zero(fockMO)
    !Transform F_mu,nu^L to F_p,q^L with L in 2ao cutoff
    loopl: do l=1, dom%nelms
       indx_l = dom%indcs(l)
       call lts_mat_init(fockMO, indx_l)
       FMO_0L_ptr => lts_get(fockMO, indx_l)
       call w_calc_mo_l(FMO_0L_ptr, indx_l, dom, fockmat, mos, wconf, lupri, luerr)
    enddo loopl
    
    !Free domain d0
    call ltdom_free(dom)

  end subroutine w_calc_AO2MO_shukla
    
  !> @brief Compute the MO fock/overlap matrix in cell L
  !> @author Elisa Rebolini
  !> @date August 2016
  !> @param 
  subroutine w_calc_mo_l(FMO_L, indx_l, dom, fockmat, cmo, wconf,lupri,luerr)
    type(lmatrixp), intent(in)                 :: fockmat, cmo
    type(matrix), intent(inout)                :: FMO_L
    type(lattice_domains_type), intent(in)     :: dom
    type(wannier_config), intent(inout)        :: wconf
    integer, intent(in)                        :: indx_l, lupri, luerr

    type(matrix) :: C_M0, C_N0, FAO_NLM, tmp
    integer      :: nbas,norb,i,j,ij
    integer      :: m, n, indx_m, indx_n
    logical      :: lstat_m, lstat_n, lstat_nlm
    real(realk)  :: lambda

    nbas=cmo%nrow
    norb=cmo%ncol
    
    
    !Initialisation 
    call mat_init(C_M0, nbas, norb)
    call mat_init(C_N0, nbas, norb)
    call mat_init(FAO_NLM, nbas, nbas)
    call mat_init(tmp, nbas, norb)
    
    call mat_zero(FMO_L)
    
    loopm: do m=1,dom%nelms; indx_m=dom%indcs(m)
       call mat_zero(C_M0)
       call lts_copy(cmo,-indx_m,lstat_m,C_M0)
       if (lstat_m) then
          loopn: do n=1,dom%nelms; indx_n=dom%indcs(n)
             call mat_zero(C_N0)
             call lts_copy(cmo,-indx_n,lstat_n,C_N0)
             if (lstat_n) then
                call mat_zero(FAO_NLM)
                call lts_copy(fockmat,indx_n-indx_m+indx_l,lstat_nlm,FAO_NLM)
                if (lstat_nlm) then
                   call mat_zero(tmp)
                   call mat_mul(FAO_NLM,C_N0,'N','N',1.0_realk,0.0_realk,tmp)
                   call mat_mul(C_M0,tmp,'T','N', 1.0_realk,1.0_realk,FMO_L)
                endif
             endif
          enddo loopn
       endif
    enddo loopm

    call mat_free(C_M0)
    call mat_free(C_N0)
    call mat_free(FAO_NLM)
    call mat_free(tmp)

  end subroutine w_calc_mo_l

    subroutine w_calc_fock_mo(blat, f_mat, cmo, FMO)
    type(bravais_lattice), intent(in) :: blat
    type(lmatrixp), intent(in) :: f_mat, cmo
    type(matrix), intent(inout) :: FMO

    type(lattice_domains_type) :: dom
    integer :: l1, l2, indx_l1, indx_l2
    type(matrix), pointer :: C_ml1, C_ml2, F_l2ml1
    type(matrix) :: tmp

    ! init domain D0
    call ltdom_init(dom, blat%dims, maxval(cmo%cutoffs))
    call ltdom_getdomain(dom, cmo%cutoffs, incl_redundant=.true.)

    ! allocate tmp matrix
    call mat_init(tmp,cmo%nrow,cmo%ncol)

    ! transform fock matrix from AO to MO basis
    call mat_zero(FMO)
    do l2 = 1, dom%nelms; indx_l2 = dom%indcs(l2)
       C_ml2 => lts_get(cmo,-indx_l2)
       if (associated(C_ml2)) then
          do l1 = 1, dom%nelms; indx_l1 = dom%indcs(l1)
             C_ml1 => lts_get(cmo,-indx_l1)
             if (associated(C_ml1)) then
                F_l2ml1 => lts_get(f_mat,abs(indx_l2-indx_l1))
                if (associated(F_l2ml1)) then
                   if (indx_l2 < indx_l1) then
                      call mat_mul(F_l2ml1,C_ml2,'T','N',1.0_realk,0.0_realk,tmp)
                   else
                      call mat_mul(F_l2ml1,C_ml2,'N','N',1.0_realk,0.0_realk,tmp)
                   endif
                   call mat_mul(C_ml1,tmp,'T','N',1.0_realk,1.0_realk,FMO)
                endif
             endif
          enddo
       endif
    enddo

    ! free tmp matrix
    call mat_free(tmp)

    ! free domain
    call ltdom_free(dom)

  end subroutine w_calc_fock_mo

  subroutine w_calc_orthpot_mo(blat, orthpot, cmo, PMO)
    type(bravais_lattice), intent(in) :: blat
    type(llmatrixp), intent(inout) :: orthpot
    type(lmatrixp), intent(in) :: cmo
    type(matrix), intent(inout) :: PMO

    type(lattice_domains_type) :: dom
    integer :: l1, l2, indx_l1, indx_l2
    type(matrix), pointer :: C_ml1, C_ml2, P_l1l2
    type(matrix) :: tmp

    ! init domain D0
    call ltdom_init(dom, blat%dims, maxval(cmo%cutoffs))
    call ltdom_getdomain(dom, cmo%cutoffs, incl_redundant=.true.)

    ! allocate tmp matrix
    call mat_init(tmp,cmo%nrow,cmo%ncol)

    ! transform orthpot matrix from AO to MO basis
    call mat_zero(PMO)
    do l2 = 1, dom%nelms; indx_l2 = dom%indcs(l2)
       C_ml2 => lts_get(cmo,-indx_l2)
       if (associated(C_ml2)) then
          do l1 = 1, dom%nelms; indx_l1 = dom%indcs(l1)
             C_ml1 => lts_get(cmo,-indx_l1)
             if (associated(C_ml1)) then
                P_l1l2 => lts_get(orthpot,indx_l1,indx_l2)
                if (associated(P_l1l2)) then
                   call mat_mul(P_l1l2,C_ml2,'N','N',1.0_realk,0.0_realk,tmp)
                   call mat_mul(C_ml1,tmp,'T','N',1.0_realk,1.0_realk,PMO)
                endif
             endif
          enddo
       endif
    enddo

    ! free tmp matrix
    call mat_free(tmp)

    ! free domain
    call ltdom_free(dom)

  end subroutine w_calc_orthpot_mo

  !> @brief Get max off-diagonal element of Fock matrix in MO basis
  !> @author Thomas Bondo Pedersen
  !> @date April 2016
  !> @param blat Bravais lattice
  !> @param f_mat Fock matrix in MO basis; LTS storage
  !> @param cmo MO coefficients; LTS storage
  !> @param lupri output unit
  !> @param luerr err unit
  function w_get_maxODF(blat, f_mat, cmo)
    real(realk) :: w_get_maxODF
    type(bravais_lattice), intent(in) :: blat
    type(lmatrixp), intent(in) :: f_mat, cmo

    type(matrix) :: FMO
    integer :: p, q, pq0
    real(realk) :: fmax

    ! allocate Fock matrix in MO basis
    call mat_init(FMO,cmo%ncol,cmo%ncol)

    ! transform AO Fock matrix to MO basis and find max off-diagonal element
    call w_calc_fock_mo(blat, f_mat, cmo, FMO)
    fmax=0.0e0_realk
    do q=1,cmo%ncol
       pq0=cmo%ncol*(q-1)
       do p=1,q-1
          fmax=max(fmax,abs(FMO%elms(pq0+p)))
       enddo
       do p=q+1,cmo%ncol
          fmax=max(fmax,abs(FMO%elms(pq0+p)))
       enddo
    enddo

    ! free Fock matrix in MO basis
    call mat_free(FMO)

    w_get_maxODF=fmax

  end function w_get_maxODF

  
end module wannier_fockMO
