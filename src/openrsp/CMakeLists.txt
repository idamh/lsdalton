# Wrapper of OpenRSP library
add_library(ls_openrsp_wrapper
            ls_openrsp_perturbations.F90
            interface_nuclear.F90
            rsp_nucpot.F90
            ls_openrsp_callback.F90
            ls_openrsp_wrapper.F90)

# Because QcMatrix library generates a header file in its build directory,
# and OpenRSP needs this header file and other header files in the
# source repository of QcMatrix library, and the macro add_external()
# does not use the command LIST_SEPARATOR to separate items, we
# respectively send these two header file directories by defining
# QCMATRIX_HEADER_DIR and PARENT_INCLUDE_DIR
#
# Therefore, the macro add_external() eases the adding of external
# projects, but it has less functionalities than directly using
# the CMake function ExternalProject_Add()
#
# See OpenRSP library documentation for the meanings of the following
# CMake arguments
set(ExternalProjectCMakeArgs
    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
    -DCMAKE_INSTALL_PREFIX=${PROJECT_BINARY_DIR}/external
    -DCMAKE_Fortran_COMPILER=${CMAKE_Fortran_COMPILER}
    -DCMAKE_Fortran_FLAGS=${CMAKE_Fortran_FLAGS}
    -DOPENRSP_USER_CONTEXT=ON
    -DOPENRSP_ZERO_BASED=OFF
    -DOPENRSP_BUILD_WEB=OFF
    -DOPENRSP_TEST_EXECUTABLE=OFF
    -DOPENRSP_FORTRAN_API=ON
    -DQCMATRIX_HEADER_DIR=${PROJECT_SOURCE_DIR}/external/qcmatrix/include
    -DQCMATRIX_MODULE_DIR=${CMAKE_Fortran_MODULE_DIRECTORY}
    -DQCMATRIX_LIB=${LS_QCMATRIX_LIBS}
    -DPARENT_INCLUDE_DIR=${PROJECT_BINARY_DIR}/external/qcmatrix-build
    #-DPARENT_MODULE_DIR=${CMAKE_Fortran_MODULE_DIRECTORY}
   )
add_external(openrsp)

unset(ExternalProjectCMakeArgs)

set(OPENRSP_HEADER_DIR
    ${PROJECT_BINARY_DIR}/external/openrsp/include)
# Because we use the same name (libopenrsp.a) as that of ls-openrsp submodule,
# we therefore install OpenRSP into another directory external/openrsp/lib
set(OPENRSP_LIB_DIR
    ${PROJECT_BINARY_DIR}/external/openrsp/lib)

include_directories(${OPENRSP_HEADER_DIR})

# Make include and library directories visibale in parent scope
set(OPENRSP_HEADER_DIR
    ${OPENRSP_HEADER_DIR}
    PARENT_SCOPE)
set(OPENRSP_LIB_DIR
    ${OPENRSP_LIB_DIR}
    PARENT_SCOPE)
set(OPENRSP_LIBS
    ${OPENRSP_LIB_DIR}/libopenrsp.a
    PARENT_SCOPE)

# Moves these dependencies to cmake/LibsLSDALTON.cmake?
add_dependencies(openrsp qcmatrix)
add_dependencies(ls_openrsp_wrapper ls_qcmatrix_wrapper)
add_dependencies(ls_openrsp_wrapper qcmatrix)
add_dependencies(ls_openrsp_wrapper openrsp)
add_dependencies(ls_openrsp_wrapper solverutillib)
add_dependencies(ls_openrsp_wrapper rspsolverlib)
