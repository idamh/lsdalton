module CGOPSOLVER
  !use direct_dens_util_unres
  use matrix_operations
  use matrix_util
  use RSPsolver
  use COMPLEXSOLVER
  use rsp_util
  private
  public :: rsp_cgop_solver, cgop_init
  integer, save :: rsp2_number_of_current_trial, rsp2_number_of_rhs, rsp2_number_of_sols, &
                 & rsp2_number_of_omegas, rsp2_number_of_startvecs,rsp2_bvec_dim
  real(realk),allocatable,save ::  ResRed(:,:)
  
contains
  subroutine cgop_init(ntrial, nrhs, nsol, nomega, nstart)
  implicit none
      integer, intent(in) :: ntrial, nrhs, nsol, nomega, nstart
      !nstart only relevant for eigenvalue problem
      !nrhs only relevant for linear equations (always 1 for eigenvalue problem)

      rsp2_number_of_current_trial = ntrial 
      rsp2_number_of_rhs = nrhs
      rsp2_number_of_sols = nsol
      rsp2_number_of_omegas = nomega
      rsp2_number_of_startvecs = nstart
      rsp2_bvec_dim = max(rsp2_number_of_current_trial,rsp2_number_of_startvecs)+3
  end subroutine cgop_init
   
subroutine rsp_cgop_solver(decomp, F, D, S,Grad, ngrad, omega, b_current)
  use decompMod
  use queue_ops4
  use COMPLEXSOLVER
    
   implicit none

! Response Solver
! (E[2]-omega*S[2])*X=-B[1]
! using conjugate gradient method with storing optimal vectors.
! keyword: .CGOP
! Only 3 last optimal vectors are needed and are stored by default,
! when pairing feature not used. 
! With pairing all vectors have to be stored. For calculation with pairing, keyword: .CGOP_PAIR, all vectors on disk
! For calculation without pairing, with all vectors on disk: .CGOP_NT
! 
!Joanna, July 2009

      type(decompItem),intent(in) :: decomp
      integer, intent(in)      :: ngrad !=1
      type(Matrix), intent(in) ::  S, F, D
      type(Matrix), intent(in) :: Grad(rsp2_number_of_sols)
      type(Matrix), intent(inout)::  b_current(rsp2_number_of_rhs) !Output
      real(realk), intent(inout) :: omega(rsp2_number_of_omegas) !first = 0
!FIFO
      TYPE(modFIFO4)           :: fifoqueue
      type(Matrix), pointer       :: xpointer,ppointer, Appointer,ApTpointer
      TYPE(modFIFO4),target        :: vectorsubspace
      
      integer                   :: ndim,max_it,maxvec,Nb_new,i,redsize,vec_truc,j,n
      type(Matrix)              :: x(rsp2_number_of_rhs),B1,sigmai(rsp2_number_of_rhs),rhoi(rsp2_number_of_rhs)
      type(Matrix)              :: res(rsp2_number_of_rhs),p
      real(realk)               :: resnorm,threshold
      type(Matrix),pointer              :: x_n,p_n,Ap_n,App_n 

   ndim = Grad(1)%nrow
   vec_truc=1!cfg_red_truc   
   call lsquit('cfg_red_truc not defined',-1)
   threshold=0.0E0_realk!cfg_rsp_conv_thr
   call lsquit('cfg_rsp_conv_thr not defined',-1)

   do j=1,ngrad
    call mat_init(x(j),ndim,ndim)   
    call mat_init(res(j),ndim,ndim)
    call mat_init(sigmai(j),ndim,ndim)
    call mat_init(rhoi(j),ndim,ndim)
  enddo
    call mat_init(B1,ndim,ndim)
    call mat_init(p,ndim,ndim)
    call lsquit('no_pairing not defined',-1)
    call lsquit('cfg_cg_truncate not defined',-1)
!    if (no_pairing) then
!        if (cfg_cg_truncate) then
!            allocate(ResRed(vec_truc,vec_truc))
!        else        
!           allocate(ResRed(cfg_rsp_maxit,cfg_rsp_maxit))
!        endif
!    else
!      allocate(ResRed(2*cfg_rsp_maxit,2*cfg_rsp_maxit))
!    endif
    
    
   if (cfg_cg_truncate) then
    call modfifo_init4(vectorsubspace,vec_truc+1)
   else
    call modfifo_init4(vectorsubspace,cfg_rsp_maxit+1)
   endif
   
      ResRed = 0.0E0_realk
   
do j=1,ngrad 
    call mat_copy(-1.0E0_realk,Grad(j),x(j))
   
     Nb_new=1
      n=0
    
    call remove_lindep(Nb_new,x(j),B1)
    call make_lintran_vecs(decomp,D,S,F,x(j),sigmai(j),rhoi(j),.true.)

    !get res
    call mat_add(1E0_realk,Grad(j),-1E0_realk,sigmai(j),res(j))
    call mat_daxpy(omega(j),rhoi(j),res(j))
    call util_scriptPx('T',D,S,res(j))

      do i=1,cfg_rsp_maxit
     
      n=n+1
      
    !get p_i-1  
    call construct_p_and_newx(decomp,vectorsubspace,D,S,F,res,Grad,ngrad,omega,n,x,p,fifoqueue)
    !get r(x_i)
    call make_lintran_vecs(decomp,D,S,F,x(j),sigmai(j),rhoi(j),.true.)
    
    !get res
    call mat_add(1E0_realk,Grad(j),-1E0_realk,sigmai(j),res(j))
    call mat_daxpy(omega(j),rhoi(j),res(j))
   
   call util_scriptPx('T',D,S,res(j)) 
    resnorm = sqrt(mat_sqnorm2(res(j)))
    write(lupri,*) 'Residual norm in iteration',i,' for vector real for frequency', omega(j), '=',resnorm
      
     if (resnorm < threshold) then 
            exit
    endif
    enddo
    enddo
    do j=1,ngrad 
  b_current(j)=x(j)
  enddo  
  
   do j=1,ngrad
  call mat_free(x(j))    
  call mat_free(res(j))
  call mat_free(sigmai(j))
  call mat_free(rhoi(j))
  enddo
  call mat_free(B1)    
  call mat_free(p)    
call modfifo_free4(vectorsubspace)
   deallocate(ResRed)
   end subroutine rsp_cgop_solver

  
  subroutine construct_p_and_newx(decomp,vectorsubspace,D,S,F,res,G,ngrad,omega,n,x,p_i,fifoqueue)
  use decompMod
  use queue_ops4
  use COMPLEXSOLVER
  implicit none
  type(decompItem),intent(in) :: decomp
  type(matrix),intent(in)      :: D,S,F
  TYPE(matrix), intent(inout)  :: p_i,x(:)
  TYPE(matrix),intent(inout) :: res(:)
  TYPE(matrix)                :: sigmai(rsp2_number_of_rhs),rhoi(rsp2_number_of_rhs)
  TYPE(matrix),intent(in)    :: G(:)
  real(realk)                :: omega(:)
  integer,intent(inout)      :: n
  integer,intent(in)      :: ngrad
  integer                    :: redsize,ndim,IERR
  TYPE(matrix)               :: pT,Ar, ArP,sigmaiT,rhoiT,r_n
  real(realk), allocatable :: alpha(:),Bmat(:,:),IPIV(:)
  TYPE(matrix)                 :: p_nT,Ap,ApT,GT,resP,scr
  integer                      :: i,j,vec_truc,k,m
 !FIFO
   TYPE(modFIFO4),intent(inout) :: fifoqueue
  type(matrix),pointer        :: xpointer,ppointer,Appointer, ApTpointer
  type(matrix),pointer        :: x_n,p_n,Ap_n,ApP_n
  type(modFIFO4),target       :: vectorsubspace 
real(realk),allocatable :: Redscr(:,:)
 
 vec_truc=cfg_red_truc 
  
   ndim = G(1)%nrow
redsize=n
!write(lupri,*) 'redsize=',n; call flshfo(lupri)

call mat_init(pT,ndim,ndim)
call mat_init(Ar,ndim,ndim)
call mat_init(ArP,ndim,ndim)
do m=1,ngrad
   call mat_init(sigmai(m),ndim,ndim)
   call mat_init(rhoi(m),ndim,ndim)
enddo
call mat_init(sigmaiT,ndim,ndim)
call mat_init(rhoiT,ndim,ndim)
call mat_init(Ap,ndim,ndim)
call mat_init(ApT,ndim,ndim)
call mat_init(GT,ndim,ndim)
call mat_init(resP,ndim,ndim)
call mat_init(scr,ndim,ndim)
call mat_init(r_n,ndim,ndim)


if (no_pairing) then
   if ((cfg_cg_truncate) .and. (redsize>=vec_truc)) then
       allocate(Bmat(vec_truc,vec_truc))
       allocate(alpha(vec_truc),IPIV(vec_truc))
       allocate(Redscr(vec_truc,vec_truc))
       Redscr=0E0_realk
    else
       allocate(Bmat(redsize,redsize))
       allocate(alpha(redsize),IPIV(redsize))
    endif
    Bmat=0E0_realk
    IPIV=0E0_realk
    alpha=0E0_realk
else
    allocate(Bmat(2*redsize,2*redsize))
    allocate(alpha(2*redsize),IPIV(2*redsize))
    Bmat=0E0_realk
    IPIV=0E0_realk
    alpha=0E0_realk
endif

do m=1,ngrad
  call mat_copy(1E0_realk,res(m),r_n)
enddo

do m=1,ngrad
    if (cfg_cgop_prec) then ! MO preconditioning by default
        call mat_copy(1E0_realk,res(m),scr)
        call MO_precond(scr,S,-omega(m),res(m),decomp%nocc)
    endif
    call make_lintran_vecs(decomp,D,S,F,res(m),sigmai(m),rhoi(m),.true.)
    call mat_add(1E0_realk,sigmai(m),-omega(m),rhoi(m),Ar)
! 
    if (.not. no_pairing) then
      call mat_trans(sigmai(m),sigmaiT)
      call mat_trans(rhoi(m),rhoiT)
      call mat_trans(G(m),GT)
      call mat_trans(res(m),resP)
      call mat_add(1E0_realk,sigmaiT,omega(m),rhoiT,ArP)
    endif

    if (no_pairing) then
       if ((cfg_cg_truncate) .and. (redsize>vec_truc)) then
            Bmat(vec_truc,vec_truc)=mat_dotproduct(res(m),Ar)
        else
            Bmat(redsize,redsize)=mat_dotproduct(res(m),Ar)
        endif
    else
          Bmat(2*redsize-1,2*redsize-1)=mat_dotproduct(res(m),Ar)
          Bmat(2*redsize,2*redsize-1)=mat_dotproduct(resP,Ar)
          Bmat(2*redsize-1,2*redsize)=mat_dotproduct(res(m),ArP)
          Bmat(2*redsize,2*redsize)=mat_dotproduct(resP,ArP)
    endif
    
  if (redsize > 1) then
        
      if (redsize>2) then
         if (no_pairing) then
             if ((cfg_cg_truncate) .and. (redsize>vec_truc)) then
                 Bmat(1:vec_truc-2,1:vec_truc-2)=ResRed(1:vec_truc-2,1:vec_truc-2)
             else
                Bmat(1:redsize-2,1:redsize-2)=ResRed(1:redsize-2,1:redsize-2)
             endif
         else
          Bmat(1:2*redsize-4,1:2*redsize-4)=ResRed(1:2*redsize-4,1:2*redsize-4)
         endif
      endif

      call mat_init(p_nT,ndim,ndim)

      if ((cfg_cg_truncate) .and. (redsize>vec_truc)) then
          call get_from_modFIFO4(vectorsubspace,vec_truc,x_n,p_n,Ap_n,ApP_n)
      else
          call get_from_modFIFO4(vectorsubspace,redsize-1,x_n,p_n,Ap_n,ApP_n)
      endif
      call mat_trans(p_n,p_nT)

      if (no_pairing) then
          if ((cfg_cg_truncate) .and. (redsize>vec_truc)) then
              Bmat(vec_truc-1,vec_truc-1)=mat_dotproduct(p_n,Ap_n)
              Bmat(vec_truc-1,vec_truc)=mat_dotproduct(p_n,Ar)
              Bmat(vec_truc,vec_truc-1)=mat_dotproduct(res(m),Ap_n)
              Redscr=ResRed
              ResRed=0E0_realk
              ResRed(vec_truc-2,vec_truc-2)=Bmat(vec_truc-1,vec_truc-1)
          else
              Bmat(redsize-1,redsize-1)=mat_dotproduct(p_n,Ap_n)
              Bmat(redsize-1,redsize)=mat_dotproduct(p_n,Ar)
              Bmat(redsize,redsize-1)=mat_dotproduct(res(m),Ap_n)
              if ((cfg_cg_truncate) .and. (redsize==vec_truc)) then
                  Redscr=ResRed
                  ResRed=0E0_realk
                  ResRed(vec_truc-2,vec_truc-2)=Bmat(redsize-1,redsize-1)
                  alpha(redsize-1)=mat_dotproduct(p_n,r_n)
              else 
                  ResRed(redsize-1,redsize-1)=Bmat(redsize-1,redsize-1)
                  alpha(redsize-1)=mat_dotproduct(p_n,r_n)
              endif
          endif
      else
         Bmat(2*redsize-3,2*redsize-3)=mat_dotproduct(p_n,Ap_n)
         Bmat(2*redsize-3,2*redsize-2)=mat_dotproduct(p_n,ApP_n)
         Bmat(2*redsize-2,2*redsize-3)=mat_dotproduct(p_nT,Ap_n)
         Bmat(2*redsize-2,2*redsize-2)=mat_dotproduct(p_nT,ApP_n)
         Bmat(2*redsize-3,2*redsize-1)=mat_dotproduct(p_n,Ar)
         Bmat(2*redsize-1,2*redsize-3)=mat_dotproduct(res(m),Ap_n)
         Bmat(2*redsize-3,2*redsize)=mat_dotproduct(p_n,ArP)
         Bmat(2*redsize,2*redsize-3)=mat_dotproduct(resP,Ap_n)
         Bmat(2*redsize-2,2*redsize-1)=mat_dotproduct(p_nT,Ar)
         Bmat(2*redsize-1,2*redsize-2)=mat_dotproduct(res(m),ApP_n)
         Bmat(2*redsize-2,2*redsize)=mat_dotproduct(p_nT,ArP)
         Bmat(2*redsize,2*redsize-2)=mat_dotproduct(resP,ApP_n)

         ResRed(2*redsize-3,2*redsize-3)=Bmat(2*redsize-3,2*redsize-3)
         ResRed(2*redsize-3,2*redsize-2)=Bmat(2*redsize-3,2*redsize-2)
         ResRed(2*redsize-2,2*redsize-3)=Bmat(2*redsize-2,2*redsize-3)
         ResRed(2*redsize-2,2*redsize-2)=Bmat(2*redsize-2,2*redsize-2)

         alpha(2*redsize-3)=mat_dotproduct(p_n,r_n)
         alpha(2*redsize-2)=mat_dotproduct(p_nT,r_n)
      endif

     if (redsize>2) then
         if ((cfg_cg_truncate) .and. (redsize>=vec_truc)) then
            if (vec_truc>3) then
                do j=1,vec_truc-3
                   do k=1,vec_truc-3
                       ResRed(j,k)=Redscr(j+1,k+1)
                   enddo
                enddo
             endif
         else
             do i=1,redsize-2    
                call get_from_modFIFO4(vectorsubspace,i,xpointer,ppointer,Appointer, ApTpointer)
                call mat_trans(ppointer,pT)
                if (no_pairing) then
                    Bmat(i,redsize-1)=mat_dotproduct(p_n,Appointer)
                    Bmat(i,redsize)=mat_dotproduct(res(m),Appointer)

                    Bmat(redsize-1,i)=mat_dotproduct(ppointer,Ap_n)
                    Bmat(redsize,i)=mat_dotproduct(ppointer,Ar)

                    ResRed(i,redsize-1)=Bmat(i,redsize-1)
                    ResRed(redsize-1,i)=Bmat(redsize-1,i)

                    alpha(i)=mat_dotproduct(ppointer,r_n)
                else
                    Bmat(2*i-1,2*redsize-3)=mat_dotproduct(p_n,Appointer)
                    Bmat(2*i-1,2*redsize-2)=mat_dotproduct(p_nT,Appointer)
                    Bmat(2*i-1,2*redsize-1)=mat_dotproduct(res(m),Appointer)
                    Bmat(2*i-1,2*redsize)=mat_dotproduct(resP,Appointer)

                    Bmat(2*i,2*redsize-3)=mat_dotproduct(p_n,ApTpointer)
                    Bmat(2*i,2*redsize-2)=mat_dotproduct(p_nT,ApTpointer)
                    Bmat(2*i,2*redsize-1)=mat_dotproduct(res(m),ApTpointer)
                    Bmat(2*i,2*redsize)=mat_dotproduct(resP,ApTpointer)

                    Bmat(2*redsize-3,2*i-1)=mat_dotproduct(ppointer,Ap_n)
                    Bmat(2*redsize-2,2*i-1)=mat_dotproduct(ppointer,ApP_n)
                    Bmat(2*redsize-1,2*i-1)=mat_dotproduct(ppointer,Ar)
                    Bmat(2*redsize,2*i-1)=mat_dotproduct(ppointer,ArP)

                    Bmat(2*redsize-3,2*i)=mat_dotproduct(pT,Ap_n)
                    Bmat(2*redsize-2,2*i)=mat_dotproduct(pT,ApP_n)
                    Bmat(2*redsize-1,2*i)=mat_dotproduct(pT,Ar)
                    Bmat(2*redsize,2*i)=mat_dotproduct(pT,ArP)

                    ResRed(2*i-1,2*redsize-3)=Bmat(2*i-1,2*redsize-3)
                    ResRed(2*i-1,2*redsize-2)=Bmat(2*i-1,2*redsize-2)
                    ResRed(2*i,2*redsize-3)=Bmat(2*i,2*redsize-3)
                    ResRed(2*i,2*redsize-2)=Bmat(2*i,2*redsize-2)
                    ResRed(2*redsize-3,2*i-1)=Bmat(2*redsize-3,2*i-1)
                    ResRed(2*redsize-2,2*i-1)=Bmat(2*redsize-2,2*i-1)
                    ResRed(2*redsize-3,2*i)=Bmat(2*redsize-3,2*i)
                    ResRed(2*redsize-2,2*i)=Bmat(2*redsize-2,2*i)

                    alpha(2*i-1)=mat_dotproduct(ppointer,r_n)
                    alpha(2*i)=mat_dotproduct(pT,r_n)
                endif
            enddo

       call mat_free(p_nT)
        endif
    endif 
endif 

if (no_pairing) then
    if ((cfg_cg_truncate) .and. (redsize>vec_truc)) then
        alpha(vec_truc) = mat_dotproduct(res(m),r_n)
     else
        alpha(redsize) = mat_dotproduct(res(m),r_n)
     endif
else
   alpha(2*redsize-1) = mat_dotproduct(res(m),r_n)
   alpha(2*redsize) = mat_dotproduct(resP,r_n)
endif
   

!write(lupri,*) 'alpha'!, alpha(1:2*redsize); call flshfo(lupri)
!if (no_pairing) then
!    if ((cfg_cg_truncate) .and. (redsize>vec_truc)) then
!        call LS_OUTPUT(alpha,1,vec_truc,1,1,1,1,1,lupri)
!        write(lupri,*) 'Bmat'!, alpha(1:2*redsize); call flshfo(lupri)
!          do i=1,vec_truc
!              write(lupri,*)(Bmat(i,j), j=1,vec_truc); call flshfo(lupri)
!          enddo
!     else
!        call LS_OUTPUT(alpha,1,redsize,1,1,1,1,1,lupri)
!        write(lupri,*) 'Bmat'!, alpha(1:2*redsize); call flshfo(lupri)
!          do i=1,redsize
!              write(lupri,*)(Bmat(i,j), j=1,redsize); call flshfo(lupri)
!          enddo
!     endif
!else
!    call LS_OUTPUT(alpha,1,2*redsize,1,1,1,1,1,lupri)
!    write(lupri,*) 'Bmat'!, alpha(1:2*redsize); call flshfo(lupri)
!          do i=1,2*redsize
!              write(lupri,*)(Bmat(i,j), j=1,2*redsize); call flshfo(lupri)
!          enddo
!endif
if (no_pairing) then
    if ((cfg_cg_truncate) .and. (redsize>vec_truc)) then
        call DGESV(vec_truc, 1, Bmat, vec_truc, IPIV, alpha, vec_truc, IERR) !Solution vector is found in RHS.
    else
        call DGESV(redsize, 1, Bmat, redsize, IPIV, alpha, redsize, IERR) !Solution vector is found in RHS.
    endif
else
    call DGESV(2*redsize, 1, Bmat, 2*redsize, IPIV, alpha, 2*redsize, IERR) !Solution vector is found in RHS.
endif
    if (IERR /= 0) then
      WRITE(LUPRI,'(/A, i4)') &
      &     'Problem in DGESV, IERR = ', IERR
      CALL LSQUIT(' Problem in DGESV',lupri)
    endif
!write(lupri,*) 'c'!, alpha(1:2*redsize); call flshfo(lupri)

!if (no_pairing) then
!   if ((cfg_cg_truncate) .and. (redsize>vec_truc)) then
!        call LS_OUTPUT(alpha,1,vec_truc,1,1,1,1,1,lupri)
!   else
!        call LS_OUTPUT(alpha,1,redsize,1,1,1,1,1,lupri)
!   endif
!else
!    call LS_OUTPUT(alpha,1,2*redsize,1,1,1,1,1,lupri)
!endif

if (no_pairing) then
    call mat_zero(p_i)
    if ((cfg_cg_truncate) .and. (redsize>vec_truc)) then
        call mat_daxpy(alpha(vec_truc),res(m),p_i)
    else
         call mat_daxpy(alpha(redsize),res(m),p_i)
    endif
else
    call mat_add(alpha(2*redsize-1),res(m),alpha(2*redsize),resP,p_i)
endif

if (redsize>1) then
    if ((cfg_cg_truncate) .and. (redsize>vec_truc)) then
        do i=2,vec_truc
            call get_from_modFIFO4(vectorsubspace,i,xpointer,ppointer,Appointer, ApTpointer)
            call mat_daxpy(alpha(i-1),ppointer,p_i)
         enddo
    else
        do i=1,redsize-1
          call get_from_modFIFO4(vectorsubspace,i,xpointer,ppointer,Appointer, ApTpointer)
       if (no_pairing) then
          call mat_daxpy(alpha(i),ppointer,p_i)
        else
          call mat_trans(ppointer,pT)
          call mat_daxpy(alpha(2*i-1),ppointer,p_i)
          call mat_daxpy(alpha(2*i),pT,p_i)
        endif
       enddo
    endif
endif   
call util_scriptPx('N',D,S,p_i) 
    call make_lintran_vecs(decomp,D,S,F,p_i,sigmai(m),rhoi(m),.true.)
    call mat_trans(sigmai(m),sigmaiT)
    call mat_trans(rhoi(m),rhoiT)
    !get Ap
    call mat_add(1E0_realk,sigmai(m),-omega(m),rhoi(m),Ap)
    !get ApP
    if (.not. no_pairing) then 
        call mat_add(1E0_realk,sigmaiT,omega(m),rhoiT,ApT)
    endif
    call mat_daxpy(1E0_realk,p_i,x(m))
     call util_scriptPx('N',D,S,x(m)) 
    call add_to_modFIFO4(vectorsubspace,x(m),p_i,Ap,ApT,0E0_realk,.false.)
    enddo
    
    call mat_free(scr)
    do m=1,ngrad
    call mat_free(sigmai(m))
    call mat_free(rhoi(m))
    enddo
    call mat_free(r_n)
    call mat_free(resP)
    call mat_free(GT)
    call mat_free(pT)
    call mat_free(Ar)
    call mat_free(ArP)
    call mat_free(sigmaiT)
    call mat_free(rhoiT)
    call mat_free(Ap)
    call mat_free(ApT)
deallocate(Bmat)
deallocate(alpha,IPIV)
   if ((cfg_cg_truncate) .and. (redsize>=vec_truc)) then
deallocate(Redscr)
endif

end subroutine construct_p_and_newx
  

subroutine remove_lindep(Nb_new,Bvec_tmp,B_scr)
!-------------------------------------------------------------------
! For zero frequency linear response, the result will
! be Z =-Y for real perturbations (antisymmetric matrix) and 
! Z = Y for imaginary perturbations (symmetric matrix) 
! ; trial vectors will have the same structure.
!
! if Z = -Y  (Y_dia=0) then the paired (Z Y) and (Y Z) are linear dep.
! we set -Y = 0,  Y_dia is already zero 
! if Z = Y then (Z Y) and (Y Z) are linear dependent
! we set  Z = 0  and Y_dia = Y_dia/2 
!---------------------------------------------------------------------
    implicit none
    integer, intent(in) :: Nb_new
    type(Matrix), intent(inout) :: Bvec_tmp
    type(Matrix), intent(inout) :: B_scr !B_scr is just scratch space
    integer :: irx,ndim
    real(realk) :: norm,norm_symm,norm_antisymm, lea
    logical :: sym_or_antisym
    real(realk),allocatable :: Be(:,:)
    
    sym_or_antisym= .false.
    ndim = B_scr%nrow
    irx=1
    !do irx = 1,Nb_new    
      !norm^2 of current matrix
      norm = mat_sqnorm2(bvec_tmp)
      !Find symmetric part of matrix
      B_scr=Bvec_tmp
      call util_get_symm_part(B_scr)
      !Find norm of symmetric part
      !if norm = 0, matrix is antisymmetric
      norm_symm = mat_outdia_sqnorm2(B_scr)
      if (info_rsp) write(lupri,*) 'remove_initial_lindep: Norm of symm part = ', norm_symm
      !test if Z + Y is zero
      if (norm_symm <= cfg_rsp_ovlmin*norm) then
        !antisymmetric matrix
        if (info_rsp) then
          WRITE(LUPRI,*) '                                       '
          WRITE(LUPRI,*) ' Z = -Y in trial vector no.',IRX
          WRITE(LUPRI,*) ' Y (or Z?) component removed and Ydia=Ydia/2.'
        endif
        sym_or_antisym = .true.
      else
        !Find antisymmetric part of matrix
        call util_get_antisymm_part(Bvec_tmp,B_scr)
        !Find norm of antisymmmetric part
        !if norm = 0, matrix is symmetric
        norm_antisymm = mat_sqnorm2(B_scr)
        if (info_rsp) write(lupri,*) 'remove_initial_lindep: ', &
                & '         Norm of asymm part = ', norm_antisymm
        if (norm_antisymm <= cfg_rsp_ovlmin*norm) then
          !symmetric matrix
          if (info_rsp) then
            WRITE(LUPRI,*) '                                       '
            WRITE(LUPRI,*) ' Z = Y in trial vector no.',IRX
            WRITE(LUPRI,*) ' Z component removed and Y_dia= Y_dia/2.'
          endif
          sym_or_antisym = .true.
        endif
      endif
      if (sym_or_antisym) then
        !set the upper triangle to zero and divide the diagonal w. 2
        
        call mat_zerohalf('UT',Bvec_tmp)
 call mat_scal_dia(0.5E0_realk,Bvec_tmp)
        sym_or_antisym = .false.
      endif
      
 end subroutine remove_lindep

 end module CGOPSOLVER
