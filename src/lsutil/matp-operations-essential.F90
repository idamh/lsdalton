!> \brief a type(Matrixp) operations module, which is used by the
!>        QcMatrix library as a backend module
!> \author Bin Gao
!> \date 2015-02-20
module matrixp_operations
    use precision, only: realk, INTK
    use files, only: lsopen, &
                     lsclose
    use Matrix_module, only: Matrix,Matrixp
    use matrix_operations, only: mat_free,           &
                                 mat_init,           &
                                 mat_get_nrow,       &
                                 mat_get_ncol,       &
                                 mat_create_block,   &
                                 mat_add_block,      &
                                 mat_retrieve_block, &
                                 mat_assign,         &
                                 mat_zero,           &
                                 mat_tr,             &
                                 mat_trAB,           &
                                 mat_dotproduct,     &
                                 mat_write_to_disk,  &
                                 mat_print,          &
                                 mat_read_from_disk, &
                                 mat_scal,           &
                                 mat_daxpy,          &
                                 mat_trans,          &
                                 mat_mul

    implicit none

    ! the following parameters should be consistent with the QcMatrix library,
    ! see the files of the QcMatrix library:
    ! # include/api/qcmatrix_f_mat_duplicate.h90
    ! # include/api/qcmatrix_f_mat_operations.h90
    ! # include/api/qcmatrix_f_mat_view.h90
    integer(kind=INTK), parameter, private :: COPY_PATTERN_AND_VALUE = 1
    integer(kind=INTK), parameter, private :: MAT_NO_OPERATION = 0
    integer(kind=INTK), parameter, private :: MAT_TRANSPOSE = 1
    integer(kind=INTK), parameter, private :: BINARY_VIEW = 0
    integer(kind=INTK), parameter, private :: ASCII_VIEW = 1

    private

    public :: Matrixp_Create
    public :: Matrixp_Associate
    public :: Matrixp_SetMat
    public :: Matrixp_GetMat
    public :: Matrixp_Destroy
    public :: Matrixp_Nullify
    public :: Matrixp_SetSymType
    public :: Matrixp_SetDimMat
    public :: Matrixp_Assemble
    public :: Matrixp_GetSymType
    public :: Matrixp_GetDimMat
    public :: Matrixp_IsAssembled
    public :: Matrixp_SetValues
    public :: Matrixp_AddValues
    public :: Matrixp_GetValues
    public :: Matrixp_Duplicate
    public :: Matrixp_ZeroEntries
    public :: Matrixp_GetTrace
    public :: Matrixp_GetMatProdTrace
    public :: Matrixp_Write
    public :: Matrixp_Read
    public :: Matrixp_Scale
    public :: Matrixp_AXPY
    public :: Matrixp_Transpose
    public :: Matrixp_GEMM

    contains

    ! all matrices should be created by this subroutine or Matrixp_Associate()
    ! or Matrixp_SetMat() before their use
    subroutine Matrixp_Create(A)
        type(Matrixp), intent(inout) :: A
        A%nrow = -1
        A%ncol = -1
        A%sym_type = 0
        A%assembled = .false.
        allocate(A%p)
        return
    end subroutine Matrixp_Create

    subroutine Matrixp_Associate(A, B)
        type(Matrixp), intent(inout) :: A
        type(Matrixp), intent(in) :: B
        A%nrow = B%nrow
        A%ncol = B%ncol
        A%sym_type = B%sym_type
        A%assembled = B%assembled
        A%p => B%p
        return
    end subroutine Matrixp_Associate

    subroutine Matrixp_SetMat(A, A_mat)
        type(Matrixp), intent(inout) :: A
        type(Matrix), target, intent(in) :: A_mat
        call mat_get_nrow(A_mat, A%nrow)
        call mat_get_ncol(A_mat, A%ncol)
        A%sym_type = 0
        A%assembled = .true.
        A%p => A_mat
        return
    end subroutine Matrixp_SetMat

    subroutine Matrixp_GetMat(A, A_mat)
        type(Matrixp), intent(in) :: A
        type(Matrix), pointer, intent(inout) :: A_mat
        A_mat => A%p
        return
    end subroutine Matrixp_GetMat

    ! all matrices should be destroyed by this subroutine or Matrixp_Nullify() after their use
    subroutine Matrixp_Destroy(A)
        type(Matrixp), intent(inout) :: A
        if (A%assembled) then
            call mat_free(A%p)
        end if
        A%nrow = -1
        A%ncol = -1
        A%sym_type = 0
        A%assembled = .false.
        deallocate(A%p)
        nullify(A%p)
        return
    end subroutine Matrixp_Destroy

    subroutine Matrixp_Nullify(A)
        type(Matrixp), intent(inout) :: A
        A%nrow = -1
        A%ncol = -1
        A%sym_type = 0
        A%assembled = .false.
        nullify(A%p)
        return
    end subroutine Matrixp_Nullify

    subroutine Matrixp_SetSymType(A, sym_type)
        type(Matrixp), intent(inout) :: A
        integer(kind=INTK), intent(in) :: sym_type
        A%sym_type = sym_type
        return
    end subroutine Matrixp_SetSymType

    subroutine Matrixp_SetDimMat(A, num_row, num_col)
        type(Matrixp), intent(inout) :: A
        integer(kind=INTK), intent(in) :: num_row
        integer(kind=INTK), intent(in) :: num_col
        A%nrow = num_row
        A%ncol = num_col
        return
    end subroutine Matrixp_SetDimMat

    ! in general, all matrices should be assembled before used in the matrix operations
    subroutine Matrixp_Assemble(A)
        type(Matrixp), intent(inout) :: A
        if (A%nrow<1) then
            call lsquit("Matrixp_Assemble>> invalid number of rows", -1_INTK)
        end if
        if (A%ncol<1) then
            call lsquit("Matrixp_Assemble>> invalid number of columns", -1_INTK)
        end if
        call mat_init(A%p, A%nrow, A%ncol)
        A%assembled = .true.
        return
    end subroutine Matrixp_Assemble

    subroutine Matrixp_GetSymType(A, sym_type)
        type(Matrixp), intent(in) :: A
        integer(kind=INTK), intent(out) :: sym_type
        sym_type = A%sym_type
        return
    end subroutine Matrixp_GetSymType

    subroutine Matrixp_GetDimMat(A, num_row, num_col)
        type(Matrixp), intent(in) :: A
        integer(kind=INTK), intent(out) :: num_row
        integer(kind=INTK), intent(out) :: num_col
        num_row = A%nrow
        num_col = A%ncol
        return
    end subroutine Matrixp_GetDimMat

    subroutine Matrixp_IsAssembled(A, assembled)
        type(Matrixp), intent(in) :: A
        logical(kind=4), intent(out) :: assembled
        assembled = A%assembled
        return
    end subroutine Matrixp_IsAssembled

    subroutine Matrixp_SetValues(A,             &
                                 idx_first_row, &
                                 num_row_set,   &
                                 idx_first_col, &
                                 num_col_set,   &
                                 values)
        type(Matrixp), intent(inout) :: A
        integer(kind=INTK), intent(in) :: idx_first_row
        integer(kind=INTK), intent(in) :: num_row_set
        integer(kind=INTK), intent(in) :: idx_first_col
        integer(kind=INTK), intent(in) :: num_col_set
        real(kind=realk), intent(in) :: values(num_row_set*num_col_set)
        if (A%assembled) then
            call mat_create_block(A%p,           &
                                  values,        &
                                  num_row_set,   &
                                  num_col_set,   &
                                  idx_first_row, &
                                  idx_first_col)
        else
            call lsquit("Matrixp_SetValues>> A is not assembled", -1_INTK)
        end if
        return
    end subroutine Matrixp_SetValues

    subroutine Matrixp_AddValues(A,             &
                                 idx_first_row, &
                                 num_row_add,   &
                                 idx_first_col, &
                                 num_col_add,   &
                                 values)
        type(Matrixp), intent(inout) :: A
        integer(kind=INTK), intent(in) :: idx_first_row
        integer(kind=INTK), intent(in) :: num_row_add
        integer(kind=INTK), intent(in) :: idx_first_col
        integer(kind=INTK), intent(in) :: num_col_add
        real(kind=realk), intent(in) :: values(num_row_add*num_col_add)
        if (A%assembled) then
            call mat_add_block(A%p,           &
                               values,        &
                               num_row_add,   &
                               num_col_add,   &
                               idx_first_row, &
                               idx_first_col)
        else
            call lsquit("Matrixp_AddValues>> A is not assembled", -1_INTK)
        end if
        return
    end subroutine Matrixp_AddValues

    subroutine Matrixp_GetValues(A,             &
                                 idx_first_row, &
                                 num_row_get,   &
                                 idx_first_col, &
                                 num_col_get,   &
                                 values)
        type(Matrixp), intent(inout) :: A
        integer(kind=INTK), intent(in) :: idx_first_row
        integer(kind=INTK), intent(in) :: num_row_get
        integer(kind=INTK), intent(in) :: idx_first_col
        integer(kind=INTK), intent(in) :: num_col_get
        real(kind=realk), intent(out) :: values(num_row_get*num_col_get)
        if (A%assembled) then
            call mat_retrieve_block(A%p,           &
                                    values,        &
                                    num_row_get,   &
                                    num_col_get,   &
                                    idx_first_row, &
                                    idx_first_col)
        else
            call lsquit("Matrixp_GetValues>> A is not assembled", -1_INTK)
        end if
        return
    end subroutine Matrixp_GetValues

    ! B should be created, but may not be assembled
    subroutine Matrixp_Duplicate(A, duplicate_option, B)
        type(Matrixp), intent(in) :: A
        integer(kind=INTK), intent(in) :: duplicate_option
        type(Matrixp), intent(inout) :: B
        ! B has been assembled before
        if (B%assembled) then
            call mat_free(B%p)
        end if
        B%nrow = A%nrow
        B%ncol = A%ncol
        B%sym_type = A%sym_type
        B%assembled = A%assembled
        if (A%assembled) then
            call mat_init(B%p, B%nrow, B%ncol)
            ! copies the values
            if (duplicate_option==COPY_PATTERN_AND_VALUE) then
                call mat_assign(B%p, A%p)
            end if
        end if
        return
    end subroutine Matrixp_Duplicate

    subroutine Matrixp_ZeroEntries(A)
        type(Matrixp), intent(inout) :: A
        if (A%assembled) then
            call mat_zero(A%p)
        else
            call lsquit("Matrixp_ZeroEntries>> A is not assembled", -1_INTK)
        end if
        return
    end subroutine Matrixp_ZeroEntries

    subroutine Matrixp_GetTrace(A, trace)
        type(Matrixp), intent(in) :: A
        real(kind=realk), intent(out) :: trace
        if (A%assembled) then
            trace = mat_tr(A%p)
        else
            call lsquit("Matrixp_GetTrace>> A is not assembled", -1_INTK)
        end if
        return
    end subroutine Matrixp_GetTrace

    subroutine Matrixp_GetMatProdTrace(A, B, op_B, trace)
        type(Matrixp), intent(in) :: A
        type(Matrixp), intent(in) :: B
        integer(kind=INTK), intent(in) :: op_B
        real(kind=realk), intent(out) :: trace
        if (.not.A%assembled) then
            call lsquit("Matrixp_GEMM>> A is not assembled", -1_INTK)
        end if
        if (.not.B%assembled) then
            call lsquit("Matrixp_GEMM>> B is not assembled", -1_INTK)
        end if
        select case (op_B)
        case (MAT_NO_OPERATION)
            if (A%ncol/=B%nrow) then
                call lsquit("Matrixp_GetMatProdTrace>> invalid dimensions (1)", &
                            -1_INTK)
            end if
            if (A%nrow/=B%ncol) then
                call lsquit("Matrixp_GetMatProdTrace>> non-square product (1)", &
                            A%nrow-B%ncol)
            end if
            ! trace = \sum_{ij}A_{ij}*B_{ji}
            trace = mat_trAB(A%p, B%p)
        case (MAT_TRANSPOSE)
            if (A%ncol/=B%ncol) then
                call lsquit("Matrixp_GetMatProdTrace>> invalid dimensions (2)", &
                            -1_INTK)
            end if
            if (A%nrow/=B%nrow) then
                call lsquit("Matrixp_GetMatProdTrace>> non-square product (2)", &
                            -1_INTK)
            end if
            ! trace = \sum_{ij}A_{ij}*B_{ij}
            trace = mat_dotproduct(A%p, B%p)
        case default
            call lsquit("Matrixp_GetMatProdTrace>> invalid operation on B", &
                        -1_INTK)
        end select
        return
    end subroutine Matrixp_GetMatProdTrace

    subroutine Matrixp_Write(A, mat_label, view_option)
        type(Matrixp), intent(in) :: A
        character*(*), intent(in) :: mat_label
        integer(kind=INTK), intent(in) :: view_option
        integer(kind=INTK) io_matrix
        if (A%assembled) then
            select case (view_option)
            ! BINARY_VIEW
            case (BINARY_VIEW)
                io_matrix = -1
                call lsopen(io_matrix,                    &
                            "qcmatrix_"//trim(mat_label), &
                            "unknown",                    &
                            "unformatted")
                call mat_write_to_disk(io_matrix, A%p)
                call lsclose(io_matrix, "KEEP")
                io_matrix = -1
                call lsopen(io_matrix,                            &
                            "qcmatrix_"//trim(mat_label)//"_dim", &
                            "unknown",                            &
                            "unformatted")
                write(io_matrix) A%nrow, A%ncol, A%sym_type
                call lsclose(io_matrix, "KEEP")
            ! ASCII_VIEW
            case (ASCII_VIEW)
                io_matrix = -1
                call lsopen(io_matrix,                    &
                            "qcmatrix_"//trim(mat_label), &
                            "unknown",                    &
                            "formatted")
                call mat_print(A%p,    &
                               1_INTK, &
                               A%nrow, &
                               1_INTK, &
                               A%ncol, &
                               io_matrix)
                call lsclose(io_matrix, "KEEP")
            case default
                call lsquit("Matrixp_Write>> invalid view option", -1_INTK)
            end select
        else
            call lsquit("Matrixp_Write>> A is not assembled", -1_INTK)
        end if
        return
    end subroutine Matrixp_Write

    ! A should be created, but may not be assembled
    subroutine Matrixp_Read(A, mat_label, view_option)
        type(Matrixp), intent(inout) :: A
        character*(*), intent(in) :: mat_label
        integer(kind=INTK), intent(in) :: view_option
        integer(kind=INTK) io_matrix
        ! A has been assembled before
        if (A%assembled) then
            call mat_free(A%p)
        end if
        select case (view_option)
        ! BINARY_VIEW
        case (BINARY_VIEW)
            io_matrix = -1
            call lsopen(io_matrix,                            &
                        "qcmatrix_"//trim(mat_label)//"_dim", &
                        "old",                                &
                        "unformatted")
            read(io_matrix) A%nrow, A%ncol, A%sym_type
            call lsclose(io_matrix, "KEEP")
            call mat_init(A%p, A%nrow, A%ncol)
            io_matrix = -1
            call lsopen(io_matrix, "qcmatrix_"//trim(mat_label), "old", "unformatted")
            call mat_read_from_disk(io_matrix, A%p, .true.)
            call lsclose(io_matrix, "KEEP")
        ! ASCII_VIEW
        case (ASCII_VIEW)
            call lsquit("Matrixp_Read>> ASCII_VIEW not supported", -1_INTK)
        case default
            call lsquit("Matrixp_Read>> invalid view option", -1_INTK)
        end select
        A%assembled = .true.
        return
    end subroutine Matrixp_Read

    subroutine Matrixp_Scale(scal_number, A)
        real(kind=realk), intent(in) :: scal_number
        type(Matrixp), intent(inout) :: A
        if (A%assembled) then
            call mat_scal(scal_number, A%p)
        else
            call lsquit("Matrixp_Scale>> A is not assembled", -1_INTK)
        end if
        return
    end subroutine Matrixp_Scale

    subroutine Matrixp_AXPY(multiplier, X, Y)
        real(kind=realk), intent(in) :: multiplier
        type(Matrixp), intent(in) :: X
        type(Matrixp), intent(inout) :: Y
        if (.not.X%assembled) then
            call lsquit("Matrixp_AXPY>> X is not assembled", -1_INTK)
        end if
        if (.not.Y%assembled) then
            call lsquit("Matrixp_AXPY>> Y is not assembled", -1_INTK)
        end if
        call mat_daxpy(multiplier, X%p, Y%p)
        Y%sym_type = X%sym_type
        return
    end subroutine Matrixp_AXPY

    ! B should be created, but may not be assembled
    subroutine Matrixp_Transpose(op_A, A, B)
        integer(kind=INTK), intent(in) :: op_A
        type(Matrixp), intent(inout) :: A
        type(Matrixp), intent(inout) :: B
        type(Matrixp) C
        if (.not.A%assembled) then
            call lsquit("Matrixp_Transpose>> A is not assembled", -1_INTK)
        end if
        select case (op_A)
        case (MAT_NO_OPERATION)
            ! A=A
            if (associated(B%p, target=A%p)) then
                return
            ! B=A
            else
                call Matrixp_Duplicate(A, COPY_PATTERN_AND_VALUE, B)
            end if
        case (MAT_TRANSPOSE)
            ! in-place transpose, we copy A to another temporary matrix C
            ! and then perform out-of-place transpose
            if (associated(B%p, target=A%p)) then
!FIXME: not work for non-square matrix, also costing extra memory here using C
                if (A%nrow/=A%ncol) then
                    call lsquit("Matrixp_Transpose>> non-square matrix not supported", &
                                -1_INTK)
                end if
                call Matrixp_Create(C)
                call Matrixp_Duplicate(A, COPY_PATTERN_AND_VALUE, C)
                call mat_trans(C%p, B%p)
                call Matrixp_Destroy(C)
            ! out-of-place transpose
            else
                ! B is not the same shape as A^{T}, or B is not assembled before
                if (B%nrow/=A%ncol .or. B%ncol/=A%nrow) then
                    ! B is assembled before
                    if (B%assembled) then
                        call mat_free(B%p)
                    end if
                    B%nrow = A%ncol
                    B%ncol = A%nrow
                    B%sym_type = A%sym_type
                    B%assembled = .true.
                    call mat_init(B%p, B%nrow, B%ncol)
                else if (.not.B%assembled) then
                    B%sym_type = A%sym_type
                    B%assembled = .true.
                    call mat_init(B%p, B%nrow, B%ncol)
                end if
                call mat_trans(A%p, B%p)
            end if
        case default
            call lsquit("Matrixp_Transpose>> invalid operation on A", -1_INTK)
        end select
        return
    end subroutine Matrixp_Transpose

    ! C should be created, but may not be assembled
    subroutine Matrixp_GEMM(op_A, op_B, alpha, A, B, beta, C)
        integer(kind=INTK), intent(in) :: op_A
        integer(kind=INTK), intent(in) :: op_B
        real(kind=realk), intent(in) :: alpha
        type(Matrixp), intent(in) :: A
        type(Matrixp), intent(in) :: B
        real(kind=realk), intent(in) :: beta
        type(Matrixp), intent(inout) :: C
        character transa, transb
        if (.not.A%assembled) then
            call lsquit("Matrixp_GEMM>> A is not assembled", -1_INTK)
        end if
        if (.not.B%assembled) then
            call lsquit("Matrixp_GEMM>> B is not assembled", -1_INTK)
        end if
        select case (op_A)
        case (MAT_NO_OPERATION)
            transa = "N"
        case (MAT_TRANSPOSE)
            transa = "T"
        case default
            call lsquit("Matrixp_GEMM>> invalid operation on A", -1_INTK)
        end select
        select case (op_B)
        case (MAT_NO_OPERATION)
            transb = "N"
        case (MAT_TRANSPOSE)
            transb = "T"
        case default
            call lsquit("Matrixp_GEMM>> invalid operation on B", -1_INTK)
        end select
        ! C is not assembled
        if (.not.C%assembled) then
            C%nrow = A%nrow
            C%ncol = B%ncol
            C%assembled = .true.
            call mat_init(C%p, C%nrow, C%ncol)
        else
            if (C%nrow/=A%nrow) then
                call lsquit("Matrixp_GEMM>> invalid number of rows", -1_INTK)
            end if
            if (C%ncol/=B%ncol) then
                call lsquit("Matrixp_GEMM>> invalid number of columns", -1_INTK)
            end if
        end if
        call mat_mul(A%p, B%p, transa, transb, alpha, beta, C%p)
        C%sym_type = 0
        return
    end subroutine Matrixp_GEMM

end module matrixp_operations
