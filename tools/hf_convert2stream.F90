!> \brief:  Read HF restart files in sequential format
!           and convert them to access stream (now default)
!           A copy of the file is made with extension .str
!> \author: Pablo Baudin
!> \date:   2015
program hf_convert2stream
   implicit none
   integer :: funit
   integer(kind=8) :: nrow8,ncol8
   logical(kind=8) :: gcbasis
   real(kind=8), allocatable :: matrix(:,:)
   character(50) :: filename
   logical :: exists

   ! density matrix file
   filename = "dens.restart"
   call convert2stream(filename,gcbasis)

   ! density matrix file
   filename = "fock.restart"
   call convert2stream(filename)

   ! density matrix file
   filename = "overlap.restart"
   call convert2stream(filename)

   ! density matrix file
   filename = "lcm_orbitals.u"
   call convert2stream(filename)

   ! density matrix file
   filename = "cmo_orbitals.u"
   call convert2stream(filename)

contains 

   subroutine convert2stream(filename,gcb)
      implicit none
      character(50), intent(inout) :: filename
      logical(kind=8), intent(inout), optional :: gcb

      integer :: funit
      integer(kind=8) :: nrow8,ncol8
      real(kind=8), allocatable :: matrix(:,:)
      logical :: exists

      ! check if file exists
      funit = 10
      inquire(file=filename,exist=exists)
      if (exists) then
         print *, "converting ",trim(filename)
      else
         print *, trim(filename), " do not exist"
         return
      end if

      ! Open and read in sequential access
      open(unit=funit, file=filename,status="unknown", FORM="unformatted")
      if (present(gcb)) then
         call read_file(funit,matrix,nrow8,ncol8,gcb=gcb) 
      else
         call read_file(funit,matrix,nrow8,ncol8) 
      end if
      close(unit=funit)

      ! Write new file with access stream and extension .str
      filename = trim(filename) // ".str"
      open(unit=funit, file=filename,status="unknown", FORM="unformatted",access='STREAM')
      call write_file(funit,matrix,nrow8,ncol8,gcb=gcbasis) 
      close(unit=funit)

   end subroutine convert2stream

   subroutine read_file(funit,matrix,nrow8,ncol8,gcb)
      implicit none
      integer, intent(in) :: funit
      real(kind=8), allocatable, intent(out) :: matrix(:,:) 
      integer(kind=8), intent(out) :: nrow8,ncol8
      logical(kind=8), optional, intent(out) :: gcb

      read(funit) nrow8,ncol8
      print*,'nrow, ncol',nrow8,ncol8
      allocate(matrix(nrow8,ncol8))
      read(funit) matrix
      if (present(gcb)) then 
         read(funit) gcb
      end if

   end subroutine read_file

   subroutine write_file(funit,matrix,nrow8,ncol8,gcb)
      implicit none
      integer, intent(in) :: funit
      real(kind=8), allocatable, intent(inout) :: matrix(:,:) 
      integer(kind=8), intent(in) :: nrow8,ncol8
      logical(kind=8), optional, intent(in) :: gcb

      write(funit) nrow8,ncol8
      write(funit) matrix
      if (present(gcb)) then 
         write(funit) gcb
      end if
      deallocate(matrix)

   end subroutine write_file

end program hf_convert2stream
